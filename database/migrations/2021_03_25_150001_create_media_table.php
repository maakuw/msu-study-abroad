<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('url');
            $table->string('thumbUrl')->default(env('FIREBASE_URL_PREFIX', './images/') . 'poster.png' . env('FIREBASE_URL_SUFFIX', ''));
            $table->string('credit')->nullable();
            $table->string('poster')->default(env('FIREBASE_URL_PREFIX', './images/') . 'poster.png' . env('FIREBASE_URL_SUFFIX', ''));
            $table->integer('type');
            $table->integer('gallery_id')->nullable();
            $table->integer('screensaver_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
