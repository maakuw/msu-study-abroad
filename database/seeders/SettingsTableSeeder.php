<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'key' => 'title',
            'title' => 'Title', 
            'description' => '',
            'value' => 'Michigan State University'
        ]);
        DB::table('settings')->insert([
            'key' => 'department',
            'title' => 'Department', 
            'description' => 'This first part of the tagline, appearing in the upper right hand side of the Header on the Screen view, is in bold font',
            'value' => "Broad College of Business"
        ]);
        DB::table('settings')->insert([
            'key' => 'program',
            'title' => 'Program', 
            'description' => 'This second part of the tagline, appearing in the upper right hand side of the Header on the Screen view, is in normal font',
            'value' => 'Education Abroad'
        ]);
        DB::table('settings')->insert([
            'key' => 'instagram',
            'title' => 'Instagram Handle', 
            'description' => 'Optional link to Instagram, appearing in the center of the Footer on the Screen view (unused)',
            'value' => 'https://instagram.com.msuabroad'
        ]);
        DB::table('settings')->insert([
            'key' => 'background',
            'title' => 'Background Image', 
            'description' => 'The fixed image which appears behind the map on the Screen view',
            'value' => '/images/background-lg.jpg',
            'type' => 'image'
        ]);
        DB::table('settings')->insert([
            'key' => 'version',
            'title' => 'Application Version', 
            'description' => 'This number iterates along with the GitHub Release Number',
            'value' => '1.0.0'
        ]);
        DB::table('settings')->insert([
            'key' => 'logo',
            'title' => 'Client Logo', 
            'description' => 'Option logo, appearing in the upper left corner of the Header on the Screen view (unused)',
            'value' => '/images/logo__michigan_state_university.svg',
            'type' => 'image'
        ]);
        DB::table('settings')->insert([
            'key' => 'greeting',
            'title' => 'Greeting Text', 
            'description' => 'The words that appear in the lower bar, above the footer on the Screen view',
            'value' => 'Touch a country to continue'
        ]);
        DB::table('settings')->insert([
            'key' => 'footer_text',
            'title' => 'Footer Link Text', 
            'description' => 'The words that appear in the Footer bar, next to the Instagram logo',
            'value' => 'MSUBROADABROAD'
        ]);
        DB::table('settings')->insert([
            'key' => 'foreground',
            'title' => 'Foreground Image', 
            'description' => 'A fixed image, occupying (roughly) 1/3rd of the screen, and appearing behind the map itself, in front of the Background, and behind the Footer and Header',
            'value' => '/images/foreground.png',
            'type' => 'image'
        ]);
        DB::table('settings')->insert([
            'key' => 'primary',
            'title' => 'Primary Color', 
            'description' => 'Used for the Header and Footer text, hover color on on table rows, Admin Sidebar menu, small buttons, and "Enabled" Countries on the Map',
            'value' => '#18453B',
            'type' => 'color'
        ]);
        DB::table('settings')->insert([
            'key' => 'secondary',
            'title' => 'Secondary Color', 
            'description' => 'Used for Table headers, Globally for anchors, labels, large buttons, and "Selected" Countries on the Map',
            'value' => '#0DB14B',
            'type' => 'color'
        ]);
        DB::table('settings')->insert([
            'key' => 'tertiary',
            'title' => 'Tertiary Color', 
            'description' => 'Used for the Navigation panels, Table borders, and anything that is a focal color but NOT a link',
            'value' => '#d0dad7',
            'type' => 'color'
        ]);
        DB::table('settings')->insert([
            'key' => 'accent1',
            'title' => 'Accent Color #2', 
            'description' => 'Used for the Breadcrumb, and any active link/button disabled by activity',
            'value' => '#C9E3D8',
            'type' => 'color'
        ]);
        DB::table('settings')->insert([
            'key' => 'accent2',
            'title' => 'Accent Color #2', 
            'description' => 'Used for Topbar and any link/button the goes OUTSIDE of the website',
            'value' => 'goldenrod',
            'type' => 'color'
        ]);
    }
}