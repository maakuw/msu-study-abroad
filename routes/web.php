<?php

namespace App\Http\Resources;
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

//Open CMS Routes
Route::get('/cms/continents', [ContinentsController::class, 'active']);
Route::get('/cms/countries', [CountriesController::class, 'active']);
Route::get('/cms/associations/countries/{id}', [CountriesController::class, 'parse']);
Route::get('/cms/associations/programs/{id}', [ProgramsController::class, 'parse']);
Route::get('/cms/countries/{slug}', [CountriesController::class, 'show']);
Route::get('/cms/galleries', [GalleriesController::class, 'index']);
Route::get('/cms/media', [MediaController::class, 'index']);
Route::get('/cms/screensaver', [ScreensaverController::class, 'index']);
Route::get('/cms/settings', [SettingsController::class, 'index']);
Route::get('/cms/programs', [ProgramsController::class, 'active']);

Route::view('/', 'base');
Route::view('/*/*/', 'base');
Route::view('/*/*/*', 'base');

//Global Authorization Routes
Auth::routes();

//Admin
Route::view('/resource/{path?}/*', 'admin')->middleware('auth');
Route::view('/admin', 'admin')->middleware('auth');
Route::view('/admin/{path?}', 'admin')->middleware('auth');
Route::view('/admin/{path?}/{subpath?}', 'admin')->middleware('auth');

//Admin Resources
Route::resources([
    '/resource/programs' => ProgramsController::class,
    '/resource/countries' => CountriesController::class,
    '/resource/continents' => ContinentsController::class,
    '/resource/galleries' => GalleriesController::class,
    '/resource/media' => MediaController::class,
    '/resource/settings' => SettingsController::class,
    '/resource/screensaver' => ScreensaverController::class
]);

Route::get('/user', [SettingsController::class, 'user']);
Route::get('/logout', [SettingsController::class, 'logout']);
Route::put('/setting', [SettingsController::class, 'setting']);