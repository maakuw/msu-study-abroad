//Take a Setting submitted through form, process the inputs so there are key pairs, then return the processed entity
export const parseSetting = (setting, settings) => {
  let update = settings.filter(s => {
    if(s.key === setting.key){
      s.updated_at = new Date(),
      s.value = setting.value
      return s
    }
  })

  return update[0]
}

export const parseSettings = (target, settings) => {
  target.title = settings[0].value
  target.department = settings[1].value
  target.program = settings[2].value
  target.instagram = settings[3].value
  target.background = settings[4].value
  target.version = settings[5].value
  target.logo = settings[6].value
  target.greeting = settings[7].value
  target.footer_text = settings[8].value
  target.foreground = settings[9].value
  target.primary = settings[10].value
  target.secondary = settings[10].value
}

export const parseProps = (target) => {
  if(target.props){
    if(target.props.settings){
      if(target.props.settings.length) parseSettings(target, target.props.settings)
    }
  }
}

export const insertSettings = (settings) => {
  let sheet = window.document.styleSheets[0]
  let root = ':root { '
  let bgPrimary = 'body .bg-primary, body .btn-primary { '
  let bgSecondary = 'body .bg-secondary, body .btn-secondary, .ant-table-content .ant-table-thead > tr > th { '
  let textPrimary = 'body .text-primary { '
  let textSecondary = 'body .text-secondary, .modal-dialog label:not(.small) { '
  let btnListItem = 'body .btn-list-item, body .bg-list-item { '
  let tertiary = 'body .bg-tertiary { '
  let accent1 = 'body .bg-seafoam { '
  let accent2 = 'body [data-topbar] { '
  root += '--screen_background: url("'+settings[4].value+'");'
  root += '--screen_foreground: url("'+settings[9].value+'");'
  root += '--bs-list-item: '+settings[10].value+'99 !important;'
  root += '--bs-primary:'+settings[10].value+' !important;'
  root += '--bs-secondary:'+settings[11].value+' !important;'
  bgPrimary += 'background-color: '+settings[10].value+' !important;'
  bgSecondary += 'background-color: '+settings[11].value+' !important; border-color: '+settings[11].value+' !important;'
  textPrimary += 'color: '+settings[10].value+' !important;'
  textSecondary += 'color: '+settings[11].value+' !important;'
  btnListItem += 'background-color: '+settings[10].value+' !important'
  tertiary += 'background-color: '+settings[12].value+' !important;'
  accent1 += 'background-color: '+settings[13].value+' !important;'
  accent2 += 'background-color: '+settings[14].value+' !important;'
  root += '}'
  bgPrimary += '}'
  bgSecondary += '}'
  textPrimary += '}'
  textSecondary += '}'
  btnListItem += '}'
  tertiary += '}'
  accent1 += '}'
  accent2 += '}'
  sheet.insertRule(root)
  sheet.insertRule(bgPrimary)
  sheet.insertRule(bgSecondary)
  sheet.insertRule(textPrimary)
  sheet.insertRule(textSecondary)
  //sheet.insertRule(btnListItem) //Causes an override of the default styles
  sheet.insertRule(tertiary)
  sheet.insertRule(accent1)
  sheet.insertRule(accent2)
}