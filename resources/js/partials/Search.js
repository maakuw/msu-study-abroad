import React, {Component} from 'react'
import {randomID} from '../functions'

class Search extends Component {
  constructor(props) {
    super(props)

    this.state = {
      error: [],
      value: ''
    }
    
    //Methods
    this.fieldChange = this.fieldChange.bind(this)
    this.clearField = this.clearField.bind(this)
    this.searchSubmit = this.searchSubmit.bind(this)
  }

  searchSubmit(value) {
    this.setState({value:value})
    this.props.searchSubmit(
     value, 
     this.props.use_countries, 
     this.props.use_programs
    )
  }

  fieldChange(event) {
    event.preventDefault()
    this.searchSubmit(event.target.value)
  }

  clearField(event) {
    event.preventDefault()
    this.searchSubmit('')
  }

  componentDidMount(){
    if(this.props) {
      if(this.props.icon) this.icon = this.props.icon
    }
  }
    
  render() {
    if( this.state.errors ) {
      this.search_response = this.state.errors
    }else{
      this.search_response = ''
    }

    return (
      <form data-search onSubmit={(e) => e.preventDefault()} className="w-100 d-flex flex-column">
        {this.search_response}
        <div className="input-group my-auto">
            { this.icon &&
            <div className="input-group-prepend position-absolute h-100" style={{left:0, zIndex:1}}>
            {!this.state.value ?
              <label className="input-group-text h-100 pe-0 ps-1 border-0" htmlFor={this.props.id ? this.props.id : this.defaultProps.id}>
                <svg className="icon text-primary">
                  <use xlinkHref={this.icon}/>
                </svg>
              </label>
            :
              <button className="input-group-text" onClick={this.clearField}>
                <svg className="icon text-primary">
                  <use xlinkHref="#icon__math--multiply"/>
                </svg>
              </button>
            }
            </div>
          }
          <input type="text" className="form-control" id={this.props.id ? this.props.id : this.defaultProps.id} style={{paddingLeft:'1.5rem'}} 
           placeholder={this.props.searchLabel ? this.props.searchLabel : this.defaultProps.searchLabel} onChange={this.fieldChange} value={this.state.value}/>
        </div>
      </form>
    )
  }
}

Search.defaultProps = {
  id: `inline_search--${randomID()}`,
  searchLabel: 'search',
  icon: '#icon__search'
}

export default Search