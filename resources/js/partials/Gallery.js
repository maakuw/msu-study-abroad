import React, {Component} from 'react'
import {Upload} from 'antd'
import Button from '../components/Button'
import cms from '../cms.json'

class Gallery extends Component {
  constructor(props) {
    super(props)

    this.state = {
      fileList: []
    }

    this.fileList = []

    this.onChange     = this.onChange.bind(this)
    this.onSuccess    = this.onSuccess.bind(this)
    this.beforeUpload = this.beforeUpload.bind(this)
  }

  onChange({file, fileList}) {
    if( this.state.is_valid ) {
      if (file.status === 'uploading') {
        this.setState({ loading: true })
      }
    }else{
      file.status = 'error'
      this.setState({loading: false})
    }
    this.setState({fileList: fileList})
  }

  onSuccess(file) {
    file.status   = 'done'
    file.percent  = 100
    let nu = this.state.fileList.map(fl => {
      if(fl.uid === file.uid){
        return file
      }else{
        return fl
      }
    })
    this.setState({fileList: nu})
    this.setState({loading: false})
    setTimeout(() => {
      //Setup for thumbnail, but hold off until time is more plentiful
      file.thumbUrl = cms.settings.firebase.url+file.name+'?alt=media'
      file.url      = cms.settings.firebase.url+file.name+'?alt=media'
    }, 10000)
  }

  beforeUpload(file){
    let isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'
    let isMov = file.type === 'video/mp4'
    let error = ''
    if (!isJpgOrPng) {
      error = 'You can only upload JPG/PNG images'
    }
    if (!isMov) {
      error = 'You can only upload MP4 videos'
    }
    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
      error = ( error.length > 0 ? error = error + ', and the i' : 'I' ) + 'mage must smaller than 2MB'
    }else{
      error += '!'
    }
    if (error.length > 0) {
      this.setState({is_valid:true})
      return true
    }else{
      this.setState({is_valid:false})
      return false
    }
  }

  componentDidUpdate(){
    console.log( this.props.files.length > 0, this.fileList !== this.state.fileList, this.fileList, this.state.fileList )
    if( this.props.files.length > 0 && this.fileList !== this.state.fileList ){
      let files = this.props.files.slice()
      //console.log(files)
      this.setState({ fileList: files})
      this.fileList = files
    }
  }

  componentDidMount(){
    if( this.props.files.length > 0 ){
      let files = this.props.files
      this.setState({ fileList: files })
      this.fileList = files
//      console.log(this.fileList)
    }
  }

  render() {
    let title = this.props.title ? this.props.title : cms.gallery.title
    if(this.props.country && this.props.id) {
      let params = {
        customRequest: (data) => {
          data.onSuccess = this.onSuccess
          this.props.uploadMedia(data, this.props.id, this.props.country)
        },
        listType: "picture-card",
        onChange: this.onChange, 
        beforeUpload: this.beforeUpload, 
        onPreview: this.props.previewModal, 
        onRemove: this.props.deleteMedia, 
        fileList: this.state.fileList, 
        iconRender: false, 
        progress: {
          strokeColor: {
            '0%': 'var(--bs-warning)',
            '100%': 'var(--bs-success)',
          },
          strokeWidth: 3,
          format: percent => `${parseFloat(percent.toFixed(2))}%`,
        }
      }
      
      return (
        <dl key={`gallery__${this.index}-${this.props.id}`} className="mb-0">
          <dt className="text-uppercase fw-bold text-secondary small mb-1 mb-md-2">{title}</dt>
          <dd className="mb-0">
            <Upload {...params}>
                <svg className="icon h2 mb-0 text-list-item">
                  <use xlinkHref="#icon__math--add"/>
                </svg>
            </Upload>
          </dd>
        </dl>
      )
    }else{
      return (
        <div className="d-flex justify-content-start align-items-center mb-1 mb-md-2">
          <h3 className="text-uppercase fw-bold text-secondary small my-auto">{title}</h3>
          <nav className="d-flex justify-content-between align-content-center ms-1 ms-md-3">
            <Button is_enabled={!this.state.loading ? true : false}
              style="primary btn-sm" label="NEW" callback={() => {
              this.props.createGallery(this.props.country.id, (gallery) => {
                this.props.setGallery(gallery)
              })
              }
            }/>
          </nav>
        </div>
      )
    }
  }
}

Gallery.defaultProps = {
  has_files: false,
  files: [],
  country: false,
  index: false,
  id: false,
  createGallery: () => {}
}

export default Gallery