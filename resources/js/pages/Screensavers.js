import cms from '../cms.json'
import React, {Component} from 'react'
import Headline from '../components/Headline'
import {Upload} from 'antd'

class Screensavers extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      fileList: []
    }

    this.has_files = false
    this.files = []

    this.onChange     = this.onChange.bind(this)
    this.onSuccess    = this.onSuccess.bind(this)
    this.beforeUpload = this.beforeUpload.bind(this)
    this.previewModal = this.previewModal.bind(this)
  }

  onChange({file, fileList}) {
    if( this.state.is_valid ) {
      if (file.status === 'uploading') {
        this.setState({ loading: true })
      }
    }else{
      file.status = 'error'
      this.setState({loading: false})
    }
    this.setState({fileList: fileList})
  }

  onSuccess(file) {
    file.status   = 'done'
    file.percent  = 100
    let nu = this.state.fileList.map(fl => {
      if(fl.uid === file.uid){
        return file
      }else{
        return fl
      }
    })
    this.setState({fileList: nu})
    this.setState({loading: false})
    setTimeout(() => {
      //Setup for thumbnail, but hold off until time is more plentiful
      file.thumbUrl = cms.settings.firebase.url+file.name+'?alt=media'
      file.url      = cms.settings.firebase.url+file.name+'?alt=media'
    }, 10000)
  }

  beforeUpload(file){
    let isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'
    let error = ''
    if (!isJpgOrPng) {
      error = 'You can only upload JPG/PNG images'
    }
    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
      error = ( error.length > 0 ? error = error + ', and the i' : 'I' ) + 'mage must smaller than 2MB'
    }else{
      error += '!'
    }
    if (error.length > 0) {
      this.setState({is_valid:true})
      return true
    }else{
      this.setState({is_valid:false})
      return false
    }
  }

  componentDidUpdate(){
    if(this.props) {   
      if(this.props.screensaver){
        if( this.props.screensaver.media.length > 0 && this.has_files === false ){
          this.setState({ fileList: this.props.screensaver.media })
          this.has_files = true
          this.files = this.props.screensaver.media
        }
      }
    }
  }

  //An Interface for the setModal that will slightly realign the display
  previewModal(file) {
    if(this.props){
      this.props.setModal("preview",file.name,'', [], false, false, file.url)
    }
  }

  render() {
    return (
      <aside key="screensavers__wrapper" className={`offcanvas offcanvas-top ${cms.screensavers.wrapper}`}
      tabIndex="-1" id="screensaver" data-bs-backdrop="false">
        <div>
        { this.props.screensaver &&
          <Upload customRequest={(data) => {
            data.onSuccess = this.onSuccess
            this.props.uploadMedia(data, 1)
          }}
          listType="picture-card" 
          onChange={this.onChange} 
          beforeUpload={this.beforeUpload} 
          onPreview={this.previewModal} 
          onRemove={this.props.deleteMedia} 
          fileList={this.state.fileList} 
          iconRender={false}  
          progress={{
            strokeColor: {
              '0%': 'var(--bs-warning)',
              '100%': 'var(--bs-success)',
            },
            strokeWidth: 3,
            format: percent => `${parseFloat(percent.toFixed(2))}%`,
          }}>
            <svg className="icon h2 mb-0 text-list-item">
              <use xlinkHref="#icon__math--add"/>
            </svg>
        </Upload>
        }
        </div>
      </aside>
    )
  }
}

export default Screensavers