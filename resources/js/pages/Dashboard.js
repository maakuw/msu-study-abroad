import React, {Component} from 'react'
import {Table} from 'antd'
import cms from '../cms.json'
import {getPosition, getLinePosition, closeModal} from '../functions'
import {findCountry, toggleCountries} from '../world'
import Button from '../components/Button'
import Breadcrumb from '../components/Breadcrumb'
import World from '../components/World'
import Subheadline from '../components/Subheadline'
import Gallery from '../partials/Gallery'
import QRCode from '../partials/QR'

class Dashboard extends Component {
  constructor(props){
    super(props)

    this.state = {
      continent: false, 
      continents: [],
      countries: [],
      country: false, 
      filter: {
        value: '', 
        label: cms.filters[0]
      },
      gallery: false, 
      loading: false, 
      programs: false,
      selectedPrograms: []
    }

    this.countries        = []
    this.programs         = false

    this.divCss           = { overflow:'hidden',overflowY:'auto' }
    this.divStyle         = "p-2 p-md-4 bg-tertiary h-100"

    this.parseContinents  = this.parseContinents.bind(this)
    this.selectContinent  = this.selectContinent.bind(this)

    this.parseCountries   = this.parseCountries.bind(this)

    this.selectCountry    = this.selectCountry.bind(this)

    this.selectFilter     = this.selectFilter.bind(this)
    this.previewModal     = this.previewModal.bind(this)

    this.getCoor          = this.getCoor.bind(this)
    this.getLine          = this.getLine.bind(this)
  }

  //An Interface for the setModal that will slightly realign the display
  previewModal(file) {
    if(this.props){
      this.props.setModal("preview",file.name,'', [], false, false, file.url)
    }
  }

  //A Wrapper function for getting and setting Text DOM element coordinates relative to the Component
  getCoor(slug, x){
    return getPosition(this,slug,x)
  }

  //A Wrapper function for getting and setting Text DOM element coordinates relative to the Component
  getLine(slug){
    return getLinePosition(this,slug)
  }

  parseContinents(){
    if(this.props.continents) {
      if(this.props.continents.length > 0 && this.state.continents.length === 0){
        this.setState({continents: this.props.continents.filter(c => c.enabled)})
      }
    }
  }

  parseCountries(countries=false){
    if(countries) {
      let enabled = countries.filter(c => c.enabled)
      this.setState({countries: enabled})
      this.countries = enabled
    }else if(this.props.countries) {
      if(this.props.countries.length > 0 && this.state.countries.length === 0){
        let enabled = this.props.countries.filter(c => c.enabled === 'on')
        this.setState({countries: enabled})
        this.countries = enabled
      }
    }
  }

  selectContinent(continent=false,callback=false){
    this.props.selectContinent(continent,callback)
    let countries = this.props.countries.filter(cou => cou.continent_id === continent.id)
    this.setState({continent: continent, countries: countries })
  }

  selectCountry(country=false){
    if(country){
      //console.log('internal selectCountry',country.programs)
      const setupCou = (c) => {
        this.setState({
          country: c, 
          selectedPrograms: [], 
          programs: c.programs,
          gallery: c.gallery
        })
        this.programs = c.programs
      }

      if(country.programs){
        this.props.selectCountry(country)
        setupCou(country)
      }else{
        this.props.selectCountry(country, (cou) => {
          setupCou(cou)
        })
      }
    }else{
      this.props.selectCountry(country)
      let state = {
        country: false,
        programs: false,
        gallery: false,
        selectedPrograms: []
      }
      let hasgrams = []
      let actives = []

      state.countries = this.state.countries.length > 0 ? this.state.countries.filter(c => {
        if(c.continent_id === this.state.continent.id && c.enabled){
          if(this.state.filter){
            if(c.programs){
              if(c.programs.length) hasgrams.push(c)
            }
            if(c.suspended === 'off') actives.push(c)
            if(this.state.filter === 'countries--active') {
              return actives
            }else if(this.state.filter === 'countries--programs') {
              return hasgrams
            }else{
              return this.countries
            }
          }
        }
      }) : []
//console.log('set state under dashboard')
      this.setState(state)
      console.log(state)
    }
  }

  selectFilter(filter=''){
    this.setState({filter: filter})
  }

  componentDidUpdate(){
    this.parseCountries()
    this.parseContinents()
  }

  componentDidMount(){
    this.parseCountries()
    this.parseContinents()
  }

  render() {
    let levels = []
    let is_admin = this.props.user_type === 1
    if(this.state.continent) levels.push(this.state.continent)
    if(this.state.country) levels.push(this.state.country)
    let programAtts = ( is_admin && this.state.programs ? (this.state.programs.length > 1  ?
    {
      rowSelection: {
        onChange: rows => {
          this.setState({ selectedPrograms: rows })
        }
      },
      rowClassName: (record) => {
        let style = "ant-table-clickable"+ (record.suspended !== 'off' ? ' suspended' : ' active')
        return style
      }
    }
    :
    {
      rowClassName: (record) => {
        let style = record.suspended !== 'off' ? 'suspended' : ''
        return style
      }
    }
    ) :
    {
      rowClassName: (record) => {
        let style = record.suspended !== 'off' ? 'suspended' : ''
        return style
      }
    } )
//    console.log(this.state.gallery)
    return (
      <article key="dashboard__wrapper" className="container mx-0 px-1 px-md-2 pb-1 pb-md-2">
        <div className="row mx-0 align-items-stretch justify-content-stretch h-100">
          <section className={`col-12 flex-column px-0 mb-1 mb-md-2 d-flex h-100${levels.length > 0 ? ' col-md-7 col-lg-8' :''}`} style={{overflow:'hidden'}}>
            <Breadcrumb levels={levels} selectFilter={this.selectFilter} filter={this.state.filter}
             selectCountry={this.selectCountry} selectContinent={this.selectContinent}/>
            <World continent={this.state.continent} continents={this.props.continents}
             country={this.state.country} countries={this.props.countries} programs={this.props.programs} 
             selectCountry={this.selectCountry} selectContinent={this.selectContinent}
             checkCountries={(slug) => { return this.props.checkCountries(slug,this,true,this.props.user_type)}}
             checkContinent={(slug) => { return this.props.checkContinent(slug,this)}} 
             getCoor={this.getCoor} getLine={this.getLine}/>
          </section>
          <section className={`flex-column align-items-stretch justify-contrent-stretch px-0 ps-md-3 pe-md-0 col-12 col-md-5 col-lg-4 mh-100 ${levels.length > 0 ? 'd-flex' : 'd-none'}`}
           style={this.divCss}>
            { this.state.continent &&
            <>
            { !this.state.country ?     
            <div className={this.divStyle} style={this.divCss}>
              <Table dataSource={this.state.countries ? this.state.countries : false}
                loading={this.state.countries.length ? false : true}
                rowKey={(record) => {
                  return `dashboard__table__row--${record.id}`
                }} 
                rowClassName={(record) => {
                  let style = "ant-table-clickable"+ (record.suspended !== 'off' ? ' suspended' : '')
                  if(record.programs) style = record.programs.length > 0 ? style + ' programs' : style
                  style = record.enabled === 'on' ? style + ' active' : style
                  return style
                }} 
                onRow={(record, r) => {
                  return {
                    onClick: (event) => {
                      if(event.target.tagName === 'TD') {
                        let country = this.props.countries.filter(c => c.id === record.id)
                        if(country.length > 0) {
                          let markup = document.getElementById(country[0].slug)
                          if(markup) {
                            toggleCountries('off', markup)
                            markup.classList.add('selected')
                            this.selectCountry(country[0])
                          }else{
                            this.props.resetAlert('There was no country to show programs for','warning')
                          }
                        }
                      }
                    }
                  }
                }}
                columns={[
                  {
                    title: 'NAME',
                    dataIndex: 'name',
                    key: 'name',
                    defaultSortOrder: 'ascend',
                    sorter: (a,b) => {
                      var left = a.name.toUpperCase()
                      var right = b.name.toUpperCase()
                      if (left < right) {
                        return -1
                      }else if (left > right) {
                        return 1
                      }else{
                        return 0
                      }
                    }
                  },{
                    title: 'Action',
                    dataIndex: 'Action',
                    key: 'Action',
                    align: 'center',
                    width: '2rem',
                    render: (text, record, index) => {
                      let gallery = this.props.galleries.filter(g => g.countries_id === record.id)
                      let media = ( record.gallery & this.props.media ) ? 
                        this.props.media.filter(media => media.gallery_id === record.gallery.id) : 
                        []
                      let s_label = record.suspended  === 'off' ? 'Suspend'     : 'Reinstate'
                      let s_style = record.suspended  === 'off' ? 'text-gold'   : 'text-success'
                      let e_label = record.enabled    === 'on'  ? 'Disable'     : 'Enable'
                      let e_style = is_admin ? ( record.enabled  === 'on' ? 'text-danger' : 'text-success' ) : 'd-none'

                      return (
                      <Button type="dropdown" is_enabled={!this.state.loading} 
                       ctas={[
                        {
                          label: 'Edit', 
                          target: '#main__modal_window', 
                          callback: () => {
                            this.props.editModal(`Edit Country #${record.id}`, '',[
                            {
                              label: 'Id',
                              id: 'id',
                              type: 'hidden',
                              value: record.id
                            },{
                              label: 'Continent Id',
                              id: 'continent_id',
                              type: 'hidden',
                              value: record.continent_id
                            },{
                              label: 'Continents',
                              id: 'continent',
                              type: 'hidden',
                              required: true, 
                              value: JSON.stringify(record.continent)
                            },{
                              label: 'Programs',
                              id: 'programs',
                              type: 'hidden',
                              required: true, 
                              value: JSON.stringify(record.programs)
                            },{
                              label: 'QR Code',
                              id: 'code',
                              type: 'hidden',  
                              value: record.code
                            },{
                              label: 'Gallery',
                              id: 'gallery',
                              type: 'hidden', 
                              value: JSON.stringify(gallery)
                            },{
                              label: 'Gallery ID',
                              id: 'gallery_id',
                              type: 'hidden',  
                              value: record.gallery_id
                            },{
                              label: 'Slug',
                              id: 'slug',
                              type: 'text', 
                              readOnly: true, 
                              required: true,  
                              style: 'col-md-6', 
                              value: record.slug
                            },{
                              label: 'Name',
                              id: 'name',
                              type: 'text', 
                              required: true,  
                              style: 'col-md-6', 
                              value: record.name
                            },{
                              label: 'Highlight Color',
                              id: 'color',
                              type: 'slider', 
                              style: 'mt-2', 
                              description: <span>This is an opacity slider. The <span className="text-primary">primary color</span> is used</span>, 
                              value: record.color
                            },{
                              label: 'Enable/Disable',
                              id: 'enabled',
                              type: is_admin ? 'checkbox' : 'hidden', 
                              style: 'col-md-6 mt-2', 
                              readOnly: this.props.user_type != 1, 
                              description: <span>This country is available to <span className="checked--invert">Moderators</span><span className="unchecked--invert">Administrators</span></span>, 
                              value: record.enabled === 'off' ? false : true
                            },{
                              label: 'Suspended/Reinstate',
                              id: 'suspended',
                              type: 'checkbox', 
                              style: is_admin ? 'col-md-6 mt-2' : 'mt-2', 
                              description: <span>This country's programs are <span className="checked">suspended</span><span className="unchecked">open</span></span>, 
                              value: record.suspended === 'off' ? false : true
                            }
                          ],(obj) => {
                            closeModal(this.props.resetModal)
                            this.props.editCountry(obj, () => {
                              let nu = this.props.countries.filter(cou => cou.continent_id === this.state.continent.id)
                              this.setState({countries: nu})
                            })
                          })}
                        },{
                          label: s_label,
                          target: '#main__modal_window', 
                          style: s_style,
                          callback: () => {
                            this.setState({loading:true})
                            this.props.editModal(`${record.suspended === 'off' ? 'Suspend' : 'Reinstate'} "${record.name}"`, 
                            record.suspended === 'off' ? cms.countries.modal.suspend : cms.countries.modal.reinstate ,[
                            {
                              label: 'id',
                              id: 'id',
                              type: 'hidden',
                              required: true, 
                              value: record.id
                            },{
                              label: 'Slug',
                              id: 'slug',
                              type: 'hidden', 
                              required: true, 
                              value: record.slug
                            },{
                              label: 'Name',
                              id: 'name',
                              type: 'hidden', 
                              required: true, 
                              value: record.name
                            },{
                              label: 'QR Code',
                              id: 'code',
                              type: 'hidden',
                              value: record.code
                            },{
                              label: 'Highlight Color',
                              id: 'color',
                              type: 'hidden',
                              value: record.color
                            },{
                              label: 'Enable/Disable',
                              id: 'enabled',
                              type: 'hidden', 
                              style: 'col-md-6 mt-2', 
                              readOnly: true, 
                              description: "This country is available to Moderators", 
                              value: record.enabled === 'off' ? false : true
                            },{
                              label: 'Suspended/Reinstated',
                              id: 'suspended',
                              type: 'hidden',
                              value: record.suspended === 'off' ? 'on' : 'off'
                            },{
                              label: 'Continent',
                              id: 'continent_id',
                              type: 'hidden',
                              required: true, 
                              value: record.continent_id
                            },{
                              label: 'Continents',
                              id: 'continent',
                              type: 'hidden',
                              required: true, 
                              value: JSON.stringify(record.continent)
                            },{
                              label: 'Programs',
                              id: 'programs',
                              type: 'hidden',
                              required: true, 
                              value: JSON.stringify(record.programs)
                            },{
                              label: 'Gallery',
                              id: 'gallery',
                              type: 'hidden', 
                              value: JSON.stringify(gallery)
                            },{
                              label: 'Gallery ID',
                              id: 'gallery_id',
                              type: 'hidden',  
                              value: record.gallery_id
                            }
                          ],(obj) => {
                            closeModal(this.props.resetModal)
                            this.props.suspendCountry(obj, () => {
                              this.setState({countries:[]})
                              let cous = this.props.countries.filter(cou => cou.continent_id === this.state.continent.id)
                              cous = cous.map(c => c.id != obj.id)
                              cous.push(obj)
                              console.log(cous)
                              this.setState({
                                countries: cous, 
                                loading: true
                              })
                            })
                          })}
                        },{
                          label: e_label,
                          target: '#main__modal_window', 
                          style: e_style,
                          callback: () => {
                            this.props.editModal(`${record.enabled === 'on' ? 'Disable' : 'Enable'} "${record.name}"`, 
                            record.enabled === 'on' ? cms.countries.modal.disabled : cms.countries.modal.enabled ,[
                            {
                              label: 'id',
                              id: 'id',
                              type: 'hidden',
                              required: true, 
                              value: record.id
                            },{
                              label: 'Slug',
                              id: 'slug',
                              type: 'hidden', 
                              required: true, 
                              value: record.slug
                            },{
                              label: 'Name',
                              id: 'name',
                              type: 'hidden', 
                              required: true, 
                              value: record.name
                            },{
                              label: 'QR Code',
                              id: 'code',
                              type: 'hidden',
                              value: record.code
                            },{
                              label: 'Highlight Color',
                              id: 'color',
                              type: 'hidden',
                              value: record.color
                            },{
                              label: 'Enable/Disable',
                              id: 'enabled',
                              type: 'hidden', 
                              style: 'col-md-6 mt-2', 
                              value: record.enabled === 'off' ? true : false
                            },{
                              label: 'Suspended/Reinstated',
                              id: 'suspended',
                              type: 'hidden',
                              value: record.suspended
                            },{
                              label: 'Continent',
                              id: 'continent_id',
                              type: 'hidden',
                              required: true, 
                              value: record.continent_id
                            },{
                              label: 'Continents',
                              id: 'continent',
                              type: 'hidden',
                              required: true, 
                              value: JSON.stringify(record.continent)
                            },{
                              label: 'Programs',
                              id: 'programs',
                              type: 'hidden',
                              required: true, 
                              value: JSON.stringify(record.programs)
                            },{
                              label: 'Gallery',
                              id: 'gallery',
                              type: 'hidden', 
                              value: JSON.stringify(gallery)
                            },{
                              label: 'Gallery ID',
                              id: 'gallery_id',
                              type: 'hidden',  
                              value: record.gallery_id
                            }
                          ],(obj) => {
                            closeModal(this.props.resetModal)
                            this.props.enableCountry(obj, () => {
                              let nu = this.props.countries.filter(cou => cou.continent_id === this.state.continent.id)
                              this.setState({countries: nu})
                            })
                          })}
                        }]} 
                        style={record.enabled === 'on' ? false : "outline-danger px-2 py-1"}/>
                      )
                    }
                  }
                ]}
                pagination={false}/>
              </div>
            :
            <>
            <Subheadline key="dashboard__headline--programs" 
            copy={this.state.country.name} h2Style="ms-0 mb-0 fw-bold text-primary display-md-6" 
            hStyle="align-items-center w-100 py-1 py-md-3 px-2 px-md-4 mx-auto bg-tertiary sticky sticky-top justify-content-start border-bottom border-2 border-md-5 border-white"/>
            <form className={this.divStyle+' mb-1 mb-md-2'} onSubmit={(event)=>{
              event.preventDefault()
              let code = document.getElementById('code')
              if(code) this.props.setQR(code.value, this.state.country)
            }}>
              <QRCode code={this.state.country.code != '' ? this.state.country.code : ''} label={cms.gallery.label}/>
            </form>
            <div className={this.divStyle+' mb-1 mb-md-2'}>
              <Gallery key="dashboard__country_gallery" 
               deleteMedia={this.props.deleteMedia} 
               uploadMedia={this.props.uploadMedia} 
               country={this.state.country} 
               createGallery={this.props.createGallery} 
               setGallery={(gallery) => {
                 this.setState({gallery:gallery})
               }}
               id={this.state.gallery ? this.state.gallery.id : false} 
               index={1} 
               previewModal={this.previewModal}
               files={ this.state.gallery ? 
                this.state.gallery.media : []}/>
            </div>
            { this.state.programs &&
            <div className="p-2 p-md-4 bg-tertiary h-100">
              <header>
                <div className="d-flex justify-content-start align-items-center mb-1 mb-md-2">
                  <h3 className="small text-uppercase fw-bold text-secondary small my-0 align-content-center">{cms.programs.label}</h3>
                  <nav className="d-flex justify-content-between align-content-center ms-1 ms-md-3">
                    { this.state.selectedPrograms.length > 0 &&
                    <button className="btn btn-sm btn-danger d-flex text-uppercase align-items-center me-1 me-md-2" onClick={() => {
                      this.setState({loading:true})
                      let programs = this.state.selectedPrograms.map(pr => {
                        let p = pr.substring(pr.search('--') + 2)
                        return this.state.programs.filter(c => c.id === parseInt(p))[0]
                      })
                      this.props.deletePrograms(programs, () => {
                        let np = this.state.programs.filter(p => {
                          return !programs.some(pp => { 
                            return pp.id === p.id
                          })
                        })
                        let cou = this.state.country
                        cou.programs = np
                        this.setState({
                          programs:np, 
                          countries: findCountry(this), 
                          country:cou,
                          selectedPrograms: [],
                          loading: false
                        })
                      })
                    }}>
                      <span className="me-1 me-md-2">DELETE</span>
                      <span className="badge rounded-pill bg-white p-1 text-primary fw-bold" style={{minWidth:'1.25rem'}}>{this.state.selectedPrograms.length}</span>
                    </button>
                    }
                    <Button type="dropdown" is_enabled={!this.state.loading} icon={'angle--down'} iStyle={
                     {
                      style: {
                        width:"0.5rem",height:"0.5rem"
                      }
                     } }   
                     label="ADD" style="primary text-uppercase btn-sm d-flex align-items-center" 
                     ctas={[
                    {
                      label: 'New',
                      target: '#main__modal_window', 
                      style: 'text-secondary',
                      callback: () => {
                        this.props.createModal(`New Program`, '',[
                          {
                            label: 'Name',
                            id: 'name',
                            type: 'text', 
                            required: true, 
                            style: 'col-12', 
                            placeholder: 'Please choose a name for the Program'
                          },{
                            label: 'Semester',
                            id: 'semester',
                            type: 'text', 
                            required: true, 
                            style: 'mt-1 mt-md-2 col-md-6', 
                            placeholder: 'Spring, Summer, Winter etc.'
                          },{
                            label: 'Country/Countries',
                            id: 'country_ids',
                            type: 'select', 
                            required: true, 
                            multiple: true, 
                            options: this.props.countries.map(co => {
                              return { 
                                label: co.name,
                                value: co.id
                              }
                            }),
                            style: 'mt-1 mt-md-2 col-md-6',
                            value: [this.state.country.id]
                          },{
                            label: 'Suspended/Open',
                            id: 'suspended',
                            type: 'checkbox', 
                            style: 'mt-2 col-12', 
                            description: <span>This program is <span className="checked">suspended</span><span className="unchecked">open</span></span>
                          }
                        ],(obj) => {
                          this.setState({loading:true})
                          closeModal(this.props.resetModal)
                          this.props.postProgram(obj, () => {
                            let cou = this.state.country
                            let nps = cou.programs ? cou.programs : []
                            this.setState({programs:[]})
                            this.setState({ 
                              programs: nps, 
                              country: cou,
                              loading: false
                            })
                          })
                        })
                     }
                    }/*,{
                      label: 'Existing',
                      target: '#main__modal_window', 
                      style: 'text-primary',
                      callback: () => {
                        this.props.editModal(`Include Existing Program`, '',[
                          {
                            label: 'Program',
                            id: 'id',
                            type: 'select', 
                            required: true, 
                            options: this.props.programs.map(pro => {
                              let exists = this.state.country.programs.find(pr => pr.id === pro.id)
                              return { 
                                label: pro.name,
                                value: pro.id,
                                selected: exists ? true : false
                              }
                            }), 
                            style: 'mt-1 mt-md-2 col'
                          }
                        ],(obj) => {
                          this.setState({loading:true})
                          let program = this.props.programs.find((p => p.id === parseInt(obj.id[0])))
                          if(program){
                            this.props.getProgramsCountries(program, (ap) => {
                              let cous = []
                              if(ap.countries.length ) {
                                cous = ap.countries.map(ac => ac.id)
                                cous.push(this.state.country.id)
                              }else{
                                cous = [this.state.country.id]
                              }
                              console.log(ap,cous)
                              ap = {
                                id: ap.id,
                                country_ids: cous,
                                name: ap.name,
                                semester: ap.semester,
                                suspended: ap.suspended
                              }
                              console.log(ap)
                              closeModal(this.props.resetModal)
                              this.props.editProgram(ap, (cou) => {
                                this.setState({
                                  programs: cou.programs, 
                                  country: cou,
                                  loading: false
                                })
                              })
                            })
                          }else{
                            this.setState({loading:false})
                          }
                        })
                      },
                    }*/]}/>
                  </nav>
                </div>
              </header>
              <Table dataSource={this.state.programs} loading={this.state.loading}
                rowKey={(record) => {
                  return `dashboard__table__row--${record.id}`
                }} {...programAtts}
                columns={[
                  {
                    title: 'Semester',
                    dataIndex: 'semester',
                    key: 'semester',
                    render: (str => {
                      return str
                    })
                  }, {
                    title: 'NAME',
                    dataIndex: 'name',
                    defaultSortOrder: 'ascend',
                    sorter: (a,b) => {
                      var left = a.name.toUpperCase()
                      var right = b.name.toUpperCase()
                      if (left < right) {
                        return -1
                      }else if (left > right) {
                        return 1
                      }else{
                        return 0
                      }
                    },
                    key: 'name'
                  },{
                    title: 'Action',
                    dataIndex: 'Action',
                    key: 'Action',
                    align: 'center',
                    width: '2rem',
                    render: (text, record, index) => {
                      return (
                      <Button type="dropdown" is_enabled={!this.state.loading}  
                       ctas={[
                        {
                          label: 'Edit', 
                          target: '#main__modal_window', 
                          callback: () => {
                            this.props.editModal(`Edit Program #${record.id}`, '',[
                            {
                              label: 'id',
                              id: 'id',
                              type: 'hidden',
                              value: record.id
                            },{
                              label: 'Countries',
                              id: 'country_ids',
                              type: 'hidden',
                              value: JSON.stringify([this.state.country.id])
                            },{
                              label: 'Name',
                              id: 'name',
                              type: 'text', 
                              required: true, 
                              style: 'col-md-6', 
                              value: record.name
                            },{
                              label: 'Semester',
                              id: 'semester',
                              type: 'text', 
                              required: true, 
                              style: 'col-md-6', 
                              value: record.semester
                            },{
                              label: 'Suspended/Open',
                              id: 'suspended',
                              type: 'checkbox', 
                              style: 'mt-1 mt-md-2', 
                              description: <span>This program is <span className="checked">suspended</span><span className="unchecked">open</span></span>, 
                              value: record.suspended === 'on' ? true : false
                            }
                          ],(obj) => {
                            this.setState({loading:true})
                            closeModal(this.props.resetModal)
                            this.props.editProgram(obj, (cou) => {
                              this.setState({
                                programs: cou.programs, 
                                country: cou,
                                loading: false
                              })
                            })
                          }) }
                        },{
                          label: 'Delete',
                          target: '#main__modal_window', 
                          style: 'text-danger',
                          callback: () => {
                            this.props.deleteModal(`Delete "${record.name}"`, '',[
                            {
                              label: 'id',
                              id: 'id',
                              type: 'hidden',
                              value: record.id
                            }
                          ],() => {
                            this.setState({loading:true})
                            closeModal(this.props.resetModal)
                            this.props.deleteProgram(record.id, () => {
                              let np = this.state.programs.filter(p => p.id !== record.id)
                              let cou = this.state.country
                              cou.programs = np
                              let cous = this.state.countries.map(c => {
                                if(c.id === this.state.country.id) {
                                  return cou
                                }else{
                                  return c
                                }
                              })
                              this.setState({
                                programs: np, 
                                countries: cous,
                                loading: false
                              })
                            })
                          }) }
                        }]}/>
                      )
                    }
                  }
                ]}
                pagination={false}/>
            </div>
            }
            </>
            }
          </>
          }
          </section>
        </div>
      </article>
    )
  }
}

Dashboard.defaultProps = {
  editProgram: () => {}, 
  checkContinent: () => {},
  checkCountries: () => {}, 
  selectCountry: () => {}, 
  postProgram: () => {}, 
  setCountry: (country=false, targ=false) => {
    if(!targ || !country) return false
    targ.setState({
      country: country, 
      selectedPrograms: [], 
      programs: country.programs,
      gallery: country.gallery
    })
    targ.programs = country.programs
  }, 
  target: false
}
export default Dashboard