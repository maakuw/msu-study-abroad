import cms from '../cms.json'
import {closeModal} from '../functions'
import React, {Component} from 'react'
import Button from '../components/Button'
import Headline from '../components/Headline'
import { Table } from 'antd'

class Programs extends Component {
  constructor(props) {
    super(props)

    this.state = ({
      programs: false, 
      loading: true, 
      selectedPrograms: []
    })

    this.deleteAll = this.deleteAll.bind(this)
  }

  //Parse the multi-selected list of programs, strip out the id, then find the Program in our list
  deleteAll() {
    let rows = this.state.selectedPrograms.map(r => {
      let row = r.substr(r.search('--') + 2)
      let entry = this.props.programs.filter(p => p.id === parseFloat(row))
      return entry[0]
    })
    this.props.deletePrograms(rows)
    this.setState({selectedPrograms: []})
  }

  componentDidUpdate(){
    if(this.props.programs && this.props.countries && !this.state.programs) {
      this.setState({programs: this.props.programs, loading: false})
    }
  }

  render() {
    return (
      <article key="article__programs" className={cms.theme.article+' overflow-scroll h-100'}>
        <Headline key="programs__headline" add_new={{
          slug: 'New', 
          is_enabled: this.state.loading, 
          callback: () => {
            this.props.createModal(`New Program`, '',[
              {
                label: 'Name',
                id: 'name',
                type: 'text', 
                required: true, 
                style: 'col-12', 
                placeholder: 'Please choose a name for the Program'
              },{
                label: 'Semester',
                id: 'semester',
                type: 'text', 
                required: true, 
                style: 'mt-2 col-md-6', 
                placeholder: 'Spring, Summer, Winter etc.'
              },{
                label: 'Country/Countries',
                id: 'country_ids',
                type: 'select', 
                required: true, 
                multiple: true, 
                options: this.props.countries.map(co => {
                  return { 
                    label: co.name,
                    value: co.id
                  }
                }),
                style: 'mt-2 col-md-6',
                value: [0]
              },{
                label: 'Suspended/Open',
                id: 'suspended',
                type: 'checkbox', 
                style: 'mt-2 col-12', 
                description: <span>This program is <span className="checked">suspended</span><span className="unchecked">open</span></span>
              }
            ],(obj) => {
              this.setState({loading:true})
              closeModal(this.props.resetModal)
              this.props.postProgram(obj, (program) => {
                let nu = this.state.programs
                this.setState({programs:[]})
                nu.push(program)
                setTimeout(() => {
                  this.setState({
                    programs:nu, 
                    loading:false
                  })
                },300)
              })
            })
         }
        }}
        has_selected={this.state.selectedPrograms.length > 0 ? 'program' : false} num_selected={this.state.selectedPrograms ? this.state.selectedPrograms.length : 0} 
        headline={cms.programs.headline} copy={cms.programs.description}
        has_search={true} searchSubmit={this.props.searchSubmit}
        use_programs={true} deleteAll={this.deleteAll}/>
        { this.state.programs &&
        <Table dataSource={this.state.programs} className={cms.components.table.style} 
          loading={ this.state.loading} 
          onChange={(pagination, filters, sorter) => {
            console.log(pagination)
          }} 
          rowKey={(record) => {
            return `programs__table__row--${record.id}`
          }}
          rowSelection={{
            onChange: rows => {
              this.setState({ selectedPrograms: rows })
            }
          }}
          rowClassName={(record) => {
            let style = "ant-table-clickable"+ (record.suspended !== 'off' ? ' suspended' : ' active')
            return style
          }}
          columns={[
            {
              title: 'Id',
              dataIndex: 'id',
              key: 'id',
              align: 'center', 
              width: '1rem', 
              defaultSortOrder: 'descend',
              sorter: (a,b) => a.id - b.id
            },{
              title: 'Semester',
              dataIndex: 'semester',
              key: 'semester', 
              width: '7rem', 
              sorter: (a,b) => {
                var left = a.semester.toUpperCase()
                var right = b.semester.toUpperCase()
                if (left < right) {
                  return -1
                }else if (left > right) {
                  return 1
                }else{
                  return 0
                }
              }
            },{
              title: 'Name',
              dataIndex: 'name',
              key: 'name',
              sorter: (a,b) => {
                var left = a.name.toUpperCase()
                var right = b.name.toUpperCase()
                if (left < right) {
                  return -1
                }else if (left > right) {
                  return 1
                }else{
                  return 0
                }
              }
            },{
              title: 'Country',
              dataIndex: 'countries',
              key: 'countries',
              render: (countries,record) => {
                if( countries ) {
                  return countries.map((cou,c) => {
                    return c + 1 < countries.length ? cou.name + ', ' : cou.name
                  })
                }else{
                  let test = this.props.getProgramsCountries(record)
                  console.log(test)
                  return 'Uh oh! Looks like you have a program assigned to a country with that doesn\'t exist!'
                }
              }
            },{
              title: 'Action',
              dataIndex: 'Action',
              key: 'Action',
              align: 'center',
              width: '2rem',
              render: (text, record, index) => {
                let programs = []
                if(record.pivot) {
                  programs = this.props.programs.filter(pr => pr.id === record.pivot.programs_id)
                  if( programs.length ) program = programs[0]
                }
                return (
                <Button type="dropdown" is_enabled={true} 
                 ctas={[
                  {
                    label: 'Edit', 
                    target: '#main__modal_window', 
                    callback: () => {
                      this.props.editModal(`Edit Program #${record.id}`, '',[
                      {
                        label: 'id',
                        id: 'id',
                        type: 'hidden',
                        value: record.id
                      },{
                        label: 'Name',
                        id: 'name',
                        type: 'text', 
                        required: true, 
                        style: 'col-12', 
                        value: record.name
                      },{
                        label: 'Semester',
                        id: 'semester',
                        type: 'text', 
                        required: true, 
                        style: 'mt-2 col-md-6', 
                        value: record.semester
                      },{
                        label: 'Country/Countries',
                        id: 'country_ids',
                        type: 'select', 
                        required: true, 
                        multiple: true, 
                        options: this.props.countries.map(co => {
                          return { 
                            label: co.name,
                            value: co.id
                          }
                        }),
                        style: 'mt-2 col-md-6',
                        value: record.countries ? record.countries.map(cou => cou.id) : []
                      },{
                        label: 'Suspended/Open',
                        id: 'suspended',
                        type: 'checkbox', 
                        style: 'mt-2', 
                        description: <span>This program is <span className="checked">suspended</span><span className="unchecked">open</span></span>, 
                        value: record.suspended === 'on' ? true : false
                      }
                    ],(obj) => {
                      closeModal(this.props.resetModal)
                      this.setState({loading: true})
                      this.props.editProgram(obj, (pr) => {
                        this.setState({programs: false})
                        let np = this.state.programs.map(p => {
                          if(p.id !== pr.id){
                            return p
                          }else{
                            return pr
                          }
                        })
                        this.setState({programs: np, loading: false})
                        })
                      })
                    }
                  },{
                    label: 'Delete',
                    target: '#main__modal_window', 
                    style: 'text-danger',
                    callback: () => {
                      this.props.deleteModal(`Delete "${record.name}"`, '',[
                        {
                          label: 'id',
                          id: 'id',
                          type: 'hidden',
                          value: record.id
                        }
                      ],() => {
                        closeModal(this.props.resetModal)
                        setTimeout(() => {this.props.deleteProgram(record.id)}, 3600)
                      })
                    }
                  }]}/>
                )
              }
            }
          ]}
          pagination={'bottom'}/>
        }
      </article>
    )
  }
}

Programs.defaultProps = {
  countries: false, 
  programs: false, 
  createModal: () => {}, 
  deleteModal: () => {}, 
  deleteProgram: () => {},
  editModal: () => {},
  editProgram: () => {}, 
  getProgramsCountries: () => {}, 
  postProgram: () => {},
  resetModal: () => {}
}

export default Programs