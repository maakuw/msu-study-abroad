import cms from '../cms.json'
import React, {Component} from 'react'
import { SketchPicker } from 'react-color'
import Button from '../components/Button'
import Headline from '../components/Headline'
import {Upload} from 'antd'

class Settings extends Component {
  constructor(props){
    super(props)

    this.formSubmit = this.formSubmit.bind(this)
  }
  
  formSubmit(event) {
    event.preventDefault()
    
    let has_error = false
    let inputs = event.target.querySelectorAll('input[type="hidden"], input[type="text"], input[type="url"]')
    let selects = event.target.querySelectorAll('select')
    let checks = event.target.querySelectorAll('input[type="checkbox"]')
    let vals = []
    
    if(checks) {
      for (var i = 0; i < checks.length; i++) {
        if(!checks[i].value && checks[i].required) has_error = true
        vals.push({
          key: checks[i].id,
          value: checks[i].checked ? 'on' : 'off'
        })
      }
    }

    if(inputs) {
      for (i = 0; i < inputs.length; i++) {
        if(!inputs[i].value && inputs[i].required) has_error = true
        vals.push({
          key: inputs[i].id,
          value: inputs[i].value
        })
      }
    }

    if(selects) {
      for (i = 0; i < selects.length; i++) {
        if(!selects[i].value && selects[i].required) has_error = true
        vals.push({
          key: selects[i].id,
          value: this.state[selects[i].id]
        })
      }
    }
    
    if(this.props.postSettings && !has_error && vals) this.props.postSettings(vals)
  }

  componentDidUpdate(){
    if(this.props){
      if(this.props.settings.length && !this.state) {
        this.props.settings.map( setting => {
          this.setState({['setting_'+setting.key]: setting.value})
        })
      }
    }
  }

  render() {
    let params = {
      customRequest: (data) => {
        data.onSuccess = this.onSuccess
      },
      listType: "picture-card",
      iconRender: false, 
      progress: {
        strokeColor: {
          '0%': 'var(--bs-warning)',
          '100%': 'var(--bs-success)',
        },
        strokeWidth: 3,
        format: percent => `${parseFloat(percent.toFixed(2))}%`,
      }
    }

    return (
      <article key="settings__article" className={cms.theme.article}>
        <Headline key="settings__headline"
        hStyle={cms.components.headline.style+' bg-white'}
        headline={cms.settings.headline} copy={cms.settings.description} />
        <form id="settings__form" className="mb-0" onSubmit={this.formSubmit}>
          <Button type="submit" label="Update"/>
          <div className="row g-3">
            { ( this.props && this.state ) &&
              this.props.settings.map( setting => {
                return (
                <div className="col-3 mb-3" key={`setting__${setting.key}`}>
                  <label className="form-label" htmlFor={setting.key}>{setting.title}</label>
                  { setting.type === 'color' ?
                  <>
                  <SketchPicker color={this.state['setting_'+setting.key]}
                  onChange={(color,evt) => {
                    this.setState({['setting_'+setting.key]:color.hex})
                  }}
                  presetColors={
                    [
                      '#0d6efd', 
                      '#6610f2',
                      '#A230CA',
                      '#f3d3e2',
                      '#FF5142',
                      '#e28b09',
                      '#FAEB5C',
                      '#27ae60',
                      '#00B9D0',
                      '#0dcaf0',
                      'white',
                      '#6c757d',
                      '#343a40',
                      '#18453B',
                      '#0DB14B',
                      '#d0dad7',
                      '#f6f7f6',
                      '#b6b6b6',
                      '#4e4e4e',
                      '#C9E3D8',
                      'rgba(24, 69, 59, 0.6)'
                    ]}/>
                  <input id={setting.key} type="hidden" 
                  value={this.state['setting_'+setting.key]}/>
                  </>
                  :/* Dev Note: needs to be tied into the Admin.js sot he image submits, and to be limited to 1 image
                  setting.type === 'image' ?
                  <Upload {...params}>
                    <svg className="icon h2 mb-0 text-list-item">
                      <use xlinkHref="#icon__math--add"/>
                    </svg>
                  </Upload>
                  :*/
                  <input id={setting.key} type={setting.type} 
                  value={this.state['setting_'+setting.key]} className="form-control"
                  onChange={(evt) => {
                    this.setState({['setting_'+setting.key]:evt.target.value})
                  }}/>
                  }
                </div>
                )
              })
            }
            <p>Additional colors are available via CSS variable names. They cannot be changed, but they can be used with the 'var(--color_name)' syntax.</p>
          </div>
          <Button type="submit" label="Update"/>
        </form>
      </article>
    )
  }
}

export default Settings