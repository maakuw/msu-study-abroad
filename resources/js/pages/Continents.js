import cms from '../cms.json'
import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'
import Headline from '../components/Headline'
import { Table } from 'antd'

class Continents extends Component {
  constructor(props) {
    super(props)

    this.state = ({
      continents: false
    })
  }

  componentDidUpdate(){
    if(this.props.continents && this.props.countries && !this.state.continents) {
      let continents = this.props.continents
      continents.forEach(con => {
        con.countries = this.props.countries.filter(cou => cou.continent_id === con.id)
      })
      console.log(continents)
      this.setState({
        continents: continents, 
        loading: false
      })
    }
  }

  render() {
    return (
      <article key="article__cms.continents." className={cms.theme.article}>
        <Headline key="cms.continents.__headline" 
        hStyle={cms.components.headline.style + ' bg-white'}
        headline={cms.continents.headline} copy={cms.continents.description}/>
        <Table dataSource={this.state.continents} className={cms.components.table.style} 
          loading={!this.state.continents ? true : false}
          rowKey={(record) => {
            return `cms.continents.__table__row--${record.id}`
          }}
          columns={[
            {
              title: 'Continents',
              dataIndex: 'name',
              key: 'name'
            },
            {
              title: 'Countries',   
              className:'inline-list',
              dataIndex: 'countries',
              key: 'countries',
              render: (countries, record) => {
//                if(!countries) {
  //                this.props.getCountries(record)
    //              return false
      //          }
                if(!countries) return <span>No Countries</span>
                let arr = this.props.countries.filter(cou => record.id === cou.continent_id)
                let links = arr.map((anchor,a) => {
                  if(anchor.enabled === 'on' || this.props.user_type === 1) {
                    return <NavLink key={`country--${a}`} to={`/admin/countries/${anchor.slug}`}>{anchor.name}</NavLink>
                  }else{
                    return <span key={`country--${a}`}>{anchor.name}</span>
                  }
                })
                return <nav className="d-flex flex-wrap w-100">{links}</nav>
              }
            }
          ]}
          pagination={false}/>
      </article>
    )
  }
}

Continents.defaultProps = {
  continents: [],
  getPage: () => {}, 
  getCountries: () => {}
}

export default Continents