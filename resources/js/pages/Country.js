import React, {Component} from 'react'
import cms from '../cms.json'
import {Spin, Table, Tooltip} from 'antd'
import {closeModal, getPaths, parseMedia, randomID} from '../functions'
import {injectProgram} from '../programs'
import Button from '../components/Button'
import Subheadline from '../components/Subheadline'
import Gallery from '../partials/Gallery'
import QRCode from '../partials/QR'
import { NavLink } from 'react-router-dom'

class Country extends Component {
  constructor(props) {
    super(props)

    this.state = {
      has_parsed: false, 
      loading: true, 
      country: false, 
      programs: [],
      selectedPrograms: []
    }

    this.paths            = ''
    this.divStyle         = "p-4 bg-white mb-2"
    
    this.previewModal     = this.previewModal.bind(this)
  }

  //An Interface for the setModal that will slightly realign the display
  previewModal(file) {
    if(this.props){
      this.props.setModal("preview",file.name,'', [], false, false, file.url ? file.url : file.thumbUrl)
    }
  }

  getMediaProps(){
    if(this.props.country.gallery){
      let obj = {
        id: this.props.country.gallery.id
      }
      let files = this.props.country.gallery.media
      console.log(files)
      if(files){
        obj.has_files = true
        obj.files = parseMedia(files)
      }else{
//          console.log('no files!')
        obj.has_files = false
        obj.files = []
      }
      return obj
    }else{
      return {}
    }
  }

  componentDidUpdate(){
    //console.log(this.props.country)
    if(this.props.country) {
      this.paths = getPaths()
      this.country = this.paths[3]
      console.log(this.country, this.props.country.slug)
      if(this.country != this.props.country.slug && !this.state.has_parsed) {
        this.setState({loading:true})
        this.props.getPage(this.country,(cou) => {
          console.log(cou)
          this.setState({country:cou, loading: false, has_parsed:true})
        })
      }else if(!this.state.country && !this.state.has_parsed){
        this.setState({country:this.props.country, loading: false, has_parsed:true})
      }
    }
  }

  render() {
    return (
      <article key="country__article" className={cms.theme.article+' d-flex px-0 align-items-stretch flex-column'} style={{ overflow:'scroll' }}>
        <nav 
          key="breadcrumb__wrapper" 
          data-breadcrumb 
          className="d-flex align-items-center bg-seafoam w-100 mb-2 sticky sticky-top" style={{zIndex:10}}>
          <div className="mb-0">
            { this.paths.length < 3 ?
            <>
            <NavLink key="breadcrumb__continent" to="/admin/continents" className="h6 fw-bold text-primary">
              <Tooltip title="Return to the Continents page">Continents</Tooltip>
            </NavLink>
            <NavLink key="breadcrumb__countries" to="/admin/countries" className="h6 fw-bold text-primary">
              <Tooltip title="Return to the Countries page">&nbsp;&rsaquo;&nbsp;Countries</Tooltip>
            </NavLink>
            </>
            :
            <NavLink key="breadcrumb__countries" to="/admin/countries" className="h6 fw-bold text-primary">
              <Tooltip title="Return to the Countries page">Countries</Tooltip>
            </NavLink>
            }
            { this.state.country &&
            <span key="breadcrumb__country" className="h6 fw-bold text-list-item">&nbsp;&rsaquo;&nbsp;{this.state.country.name}</span>
            }
          </div>
        </nav>
        <section className="d-flex flex-wrap h-100 pb-2 px-2" style={{zIndex:5}}>
          <div className="col-12 col-lg-5">
            <header key="country_headline" data-headline className={cms.components.headline.style+' bg-white'} style={{overflow:'visible'}}>
              <h1 className="ms-0">{ this.state.country && this.state.country.name }</h1>
              { this.state.country &&
              <form className="d-flex flex-column justify-content-start align-items-center" onSubmit={(event)=>{
                event.preventDefault()
                let name  = document.getElementById('name'), 
                slug      = document.getElementById('slug'), 
                label_x   = document.getElementById('label_x'), 
                label_y   = document.getElementById('label_y'), 
                line_x1   = document.getElementById('line_x1'), 
                line_y1   = document.getElementById('line_y1'), 
                line_x2   = document.getElementById('line_x2'), 
                line_y2   = document.getElementById('line_y2'),
                country   = {
                  name: name.value, 
                  slug: slug.value, 
                  label_x: label_x.value ? Number(label_x.value) : null,
                  label_y: label_y.value ? Number(label_y.value) : null,
                  line_x1: line_x1.value ? Number(line_x1.value) : null,
                  line_y1: line_y1.value ? Number(line_y1.value) : null,
                  line_x2: line_x2.value ? Number(line_x2.value) : null,
                  line_y2: line_y2.value ? Number(line_y2.value) : null,
                }
                this.props.setDetails(country, this.state.country)
              }}>
                <input id="slug" type="hidden" defaultValue={this.state.country.slug}/>
                <fieldset key="country_details" className="w-100 row ps-md-0 mb-3">
                  <label className="col-6 col-md-8 small">
                    <span className="small">Name</span>
                    <input id="name" className="w-100" defaultValue={this.state.country.name}/>
                  </label>
                  <label className="col-3 col-md-2 small">
                    <span className="small">Label X</span>
                    <input id="label_x" className="w-100" defaultValue={this.state.country.label_x}/>
                  </label>
                  <label className="col-3 col-md-2 small">
                  <span className="small">Label Y</span>
                    <input id="label_y" className="w-100" defaultValue={this.state.country.label_y}/>
                  </label>
                </fieldset>
                <fieldset key="country_line" className="w-100 row ps-md-0 mb-3">
                  <legend>Label Line</legend>
                  <label className="col-6 col-md-3 small">
                    <span className="small">Starting Point X</span>
                    <input id="line_x1" className="w-100" defaultValue={this.state.country.line_x1}/>
                  </label>
                  <label className="col-6 col-md-3 small">
                    <span className="small">Starting Point Y</span>
                    <input id="line_y1" className="w-100" defaultValue={this.state.country.line_y1}/>
                  </label>
                  <label className="col-6 col-md-3 small">
                    <span className="small">Ending Point X</span>
                    <input id="line_x2" className="w-100" defaultValue={this.state.country.line_x2}/>
                  </label>
                  <label className="col-6 col-md-3 small">
                    <span className="small">Ending Point Y</span>
                    <input id="line_y2" className="w-100" defaultValue={this.state.country.line_y2}/>
                  </label>
                </fieldset>
                <nav className="w-100 d-flex justify-content-end">
                  <button className="btn btn-primary text-uppercase rounded-0 rounded-end btn-sm" type="submit">Update</button>
                </nav>
              </form>
              }
            </header>
            <form className={this.divStyle} onSubmit={(event)=>{
              event.preventDefault()
              let code =  document.getElementById('code')
              if(code) this.props.setQR(code.value, this.state.country)
              }}>
              <QRCode key="qr_code" code={this.state.country && this.state.country.code}/>
            </form>
            <div className={this.divStyle}>
              <Gallery key="country_gallery" 
                index={this.props.country.id} 
                country={this.props.country} 
                createGallery={this.props.createGallery} 
                previewModal={this.previewModal}
                deleteMedia={this.props.deleteMedia}  
                uploadMedia={this.props.uploadMedia}
                {...this.getMediaProps()}/>
            </div>
          </div>
          <div className="col-12 col-lg-7 bg-tertiary">
            <div className="p-4 h-100">
              <Subheadline key="country_subheadline" hStyle={cms.components.headline.style+' bg-tertiary mb-4 pt-0'}
              copy={'programs'} add_new={{
                slug: 'New',
                callback: () => {
                  this.props.createModal(`New Program`, '',[
                  {
                    label: 'Name',
                    id: 'name',
                    type: 'text', 
                    required: true, 
                    style: 'col-md-6'
                  },{
                    label: 'Semester',
                    id: 'semester',
                    type: 'text', 
                    required: true, 
                    style: 'col-md-6'
                  },{
                    label: 'Country',
                    id: 'country_id',
                    type: 'select', 
                    options: this.props.countries.map(co => {
                      return { 
                        label: co.name,
                        value: co.id
                      }
                    }),
                    readOnly: true,
                    style: 'mt-2 col-md-6',
                    value: this.props.countries.filter(c => c.id === this.props.country.id)[0].id
                  },{
                    label: 'Suspended/Open',
                    id: 'suspended',
                    type: 'checkbox', 
                    style: 'mt-2 col-md-6', 
                    description: <span>This program is <span className="checked">suspended</span><span className="unchecked">open</span></span>, 
                  }
                ],(obj) => {
                  injectProgram(obj,this)
                  closeModal(this.props.resetModal)
                  this.props.postProgram(obj)
                  })
                }
              }} has_selected={this.state.selectedPrograms.length > 0 ? 'program' : false} num_selected={this.state.selectedPrograms ? this.state.selectedPrograms.length : 0} />
              <Table dataSource={this.state.country && this.state.country.programs} className={cms.components.table.style + ' px-0'} 
                rowKey={(record) => {
                  return `country__table__row--${record.id}`
                }} 
                rowSelection={{
                  onChange: rows => {
                    this.setState({ selectedPrograms: rows })
                  }
                }}
                columns={[
                  {
                    title: 'Semester',
                    dataIndex: 'semester',
                    key: 'semester',
                    render: (str => {
                      return str
                    })
                  },{
                    title: 'NAME',
                    dataIndex: 'name',
                    key: 'name'
                  },{
                    title: 'Action',
                    dataIndex: 'Action',
                    key: 'Action',
                    align: 'center',
                    width: '2rem',
                    render: (text, record, index) => {
                      return (
                      <Button type="dropdown" is_enabled={true} 
                        ctas={[
                        {
                          label: 'Edit', 
                          target: '#main__modal_window', 
                          callback: () => {
                            this.props.editModal(`Edit Program #${record.id}`, '',[
                            {
                              label: 'id',
                              id: 'id',
                              type: 'hidden',
                              value: record.id
                            },{
                              label: 'Country',
                              id: 'country_id',
                              type: 'hidden',
                              value: record.country_id
                            },{
                              label: 'Name',
                              id: 'name',
                              type: 'text', 
                              required: true, 
                              style: 'col-md-6', 
                              value: record.name
                            },{
                              label: 'Semester',
                              id: 'semester',
                              type: 'text', 
                              required: true, 
                              style: 'col-md-6', 
                              value: record.semester
                            },{
                              label: 'Suspended/Open',
                              id: 'suspended',
                              type: 'checkbox', 
                              style: 'mt-2', 
                              description: <span>This program is <span className="checked">suspended</span><span className="unchecked">open</span></span>, 
                              value: record.suspended === 'on' ? true : false
                            }
                          ],(obj) => {
                            closeModal(this.props.resetModal)
                            injectProgram(obj, this)
                            this.props.editProgram(obj)
                            })
                          }
                        },{
                          label: 'Delete',
                          target: '#main__modal_window', 
                          style: 'text-danger',
                          callback: () => {
                            this.props.deleteModal(`Delete "${record.name}"`, '',[
                            {
                              label: 'id',
                              id: 'id',
                              type: 'hidden',
                              value: record.id
                            }
                          ],() => {
                            closeModal(this.props.resetModal)
                            let np = this.state.programs.filter(p => p.id !== record.id)
                            this.props.deleteProgram(record.id)
                            this.setState({programs: np})
                            })
                          }
                        }]}/>
                      )
                    }
                  }
                ]}
                pagination={false} loading={this.state.loading}/>
            </div>
          </div>
        </section>
      </article>
    )
  }
}

Country.defaultProps = {
  country: false,
  countries: [],
  createGallery: () => {},
  editProgram: () => {},
  editModal: () => {},
  deleteMedia: () => {},
  deleteProgram: () => {},
  deleteModal: () => {},
  postProgram: () => {},
  getPage: () => {},
  setDetails: () => {},
  setQR: () => {},
  uploadMedia: () => {},
}

export default Country