import cms from '../cms.json'
import React, {Component} from 'react'
import {closeModal} from '../functions'
import Gallery from '../partials/Gallery'
import Headline from '../components/Headline'

class Galleries extends Component {
  constructor(props) {
    super(props)

    this.state = {
      galleries: false
    }

    this.galleries = []

    this.previewModal = this.previewModal.bind(this)
  }

  //An Interface for the setModal that will slightly realign the display
  previewModal(file) {
    this.props.setModal("preview",file.name,'', [], false, false, file.url)
  }

  componentDidUpdate(){
    if(this.props.countries && !this.state.galleries || this.props.galleries != this.state.galleries ){
      this.setState({galleries:this.props.galleries})
    }
  }

  render() {
    return (
      <article key="galleries__article" className={cms.theme.article}> 
        <Headline key="galleries__headline" 
        hStyle={cms.components.headline.style+' bg-white'}
        headline={cms.galleries.headline} copy={cms.galleries.description} 
        add_new={{
        slug: 'New',
        is_enabled: this.state.loading, 
        callback: () => {
          this.props.createModal(`New Gallery`, 'Please select a country/countries to associate your new gallery to',[
            {
              label: 'Country/Countries',
              id: 'countries_id',
              type: 'select', 
              required: true, 
              multiple: false, 
              options: this.props.countries.map(co => {
                return { 
                  label: co.name,
                  value: co.id
                }
              }),
              style: 'mt-2 col-md-6'
            }
          ],(obj) => {
            this.setState({loading:true})
            closeModal(this.props.resetModal)
            //Dev Note This will be pulled out once galleries use multiple countires
            let id = obj.countries_id[0]
            this.props.createGallery(parseInt(id), (galleries) => {
              this.setState({galleries:galleries})
              //this.setGallery(obj.id, obj)
            })
          })}
        }}/>
        { this.state.galleries &&
        <div className="px-3 overflow-scroll h-100">
        { this.state.galleries.map((gallery, c) => {
          let country = this.props.countries.find((cou) => cou.id === gallery.countries_id)
          let files = this.props.media.filter(media => media.gallery_id === gallery.id)
          //console.log(country, files)
          return (
            <Gallery key={`gallery--${c}`}
              deleteMedia={this.props.deleteMedia} 
              uploadMedia={this.props.uploadMedia} 
              country={country} 
              index={c} 
              previewModal={this.previewModal}
              files={files}
              title={country ? country.name : `Unnamed [${c}]`} 
              id={gallery.id}/>
          )
          })
        }
        </div>
        }
      </article>
    )
  }
}

Galleries.defaultProps = {
  countries: false, 
  galleries: false, 
  media: false, 
  deleteMedia: () => {}, 
  uploadMedia: () => {}, 
  previewModal: () => {}, 
  setGallery: (index, gallery) => {
    this.setState({
      ['gallery_'+index]: gallery
    })
  },
  setModal: () => {}
}

export default Galleries