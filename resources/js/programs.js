import api from './api'

//Look at the list of Programs, and add classes to any Map components
export const checkPrograms = (country) => {
  //console.log('checkPrograms', country)
  if(!country) return 'noprograms'
  return country.programs.length ? 'programs' : 'noprograms'
}

//Find an existing Program and put new input into it
export const updateProgram = (program, target) => {
  return target.programs.map(p => {
    if(p.id !== program.id){
      return p
    }else{
      return program
    }
  })
}

//Push a new program into the state 'reset' variable and the state of a Component
export const injectProgram = (program, target) => {
  let state = target.state
  let programs = state.programs
  target.setState({programs: []})
  programs.push(program)
  setTimeout(() => {target.setState({programs: programs})},300)
  target.programs = programs
}

//Take a program submitted through a Modal form, process the inputs, then return the processed entity
export const parseProgram = (program) => {
  if(program.id) program.id = parseFloat(program.id)
  program.countries = typeof program.country_ids != 'string' ? 
                      JSON.stringify(program.country_ids) : 
                      program.country_ids
  delete program.country_ids
  return program
}

export const getPrograms = (target, callback=false) => {
  target.setState({loading:true})
  api.get('/cms/programs')
  .then(response => {
    target.resetAlert()
    target.setState({
      programs: response.data,
      feedback: {
        msg: 'Programs successfully loaded!',
        style: 'success'
      },
      is_loaded: true
    })
    target.programs = response.data
  })
  .finally((e) => {
    if(callback) callback()
  })
  .catch(error => {
    target.setState({
      feedback: {
        msg: `And error occurred when loading the Programs! ${error.data}`,
        style: 'danger'
      }
    })
  })
}