import {getPaths} from './functions'

export const toggleContinents = (status='on', current=false) => {
  let continents = document.querySelectorAll('[data-continent]')
  if(current) {
    continents.forEach(c => {
      if(c !== current ){
        status === 'off' ? c.classList.add('disabled') : c.classList.remove('disabled')
        c.classList.remove('selected')
      }
    })
  }else{
    continents.forEach(c => {
      c.classList.remove('selected','disabled')
    })
  }
}

export const toggleCountries = (status='on', current=false) => {
  let countries = document.querySelectorAll('[data-country]')
  if(current) {
    countries.forEach(c => {
      if(c !== current ) {
        //status === 'off' ? c.classList.add('disabled') : c.classList.remove('disabled')
        c.classList.remove('selected')
      }
    })
  }else{
    countries.forEach(c => {
      c.classList.remove('selected')
    })
  }
}

export const touchContinent = (event, target) => {
  //console.log(event, target)
  let path      = event.target
  let country   = path.parentNode
  //Ensure that countries that are NOT part of continents still clickthrough
  if( country.nodeName !== 'g') country = country.parentNode
  let continent = country.parentNode
  let slug      = continent.attributes.id.nodeValue
  let obj       = target.props.continents.filter(c=>c.slug === slug)[0]
  let paths     = getPaths()

  toggleContinents('off',continent)
  target.props.selectContinent(obj,() => {
    //console.log(continent)
    if(!country.classList.contains('active') || paths[1]) {
      zoomMap(continent.dataset.scale, 
                  continent.dataset.transformoriginx, 
                  continent.dataset.transformoriginy)
    }

    continent.classList.add('selected')
  })
}

export const touchCountry = (event, target, callback=false) => {
  let path      = event.target
  let country   = path.parentNode
  if( country.nodeName !== 'g') country = country.parentNode
  if(country.attributes.id && country.className) {
    let slug      = country.attributes.id.nodeValue
    let obj       = target.props.countries.filter(c=>c.slug === slug && ( c.suspended === 'off' || target.props.show_suspended))
    
    toggleCountries('off',country)
    country.classList.add('selected')

    if(obj.length > 0) target.props.selectCountry(obj[0])
    if(callback) callback(obj[0])
  }
}

//Look at the list of Countries, and class them based on existence and suspension
//Used by the Map component for style and listeners
export const checkContinent = (slug = '', target) => {
  if(!target) return ''
  let exists = target.state.continents.filter(c => c.slug === slug)
  return exists.length ? 'active' : ''
}

//Look at the list of Countries, and class them based on existence and suspension
//Used by the Map component for style and listeners, both on Screen.js and Dashboard.js
export const checkCountries = (slug='', target=false, use_admin=false, user_type=0) => {
  if(!target) return ''
  if(target.state.countries.length <= 0) return ''
  let exists = target.state.countries.filter(c => c.slug === slug)
  if(exists.length > 0) {
    exists = exists[0]
    if(exists.enabled === 'on' || (use_admin & user_type === 1)) {
      let styles = ''
      if(exists.suspended === 'off'){
        styles =  'active'
        styles += ' opacity-'+exists.color
      }else{
        styles = use_admin ? 'active suspended' : 'suspended'
//        console.log(styles)
      }
      return styles
    }else{
      return 'disabled'
    }
  }else{
    return ''
  }
}

export const findCountry = (target) => {
  if(!target.state) return false
  if(!target.state.countries) return false
  target.state.countries.map(c => c.id === target.state.country.id)
}

export const zoomMap = (scale,originX,originY) => {
  let map = document.querySelector('[data-map]')
  if(scale){
    map.style.transform = `scale(${scale}) translate(${originX}vw, ${originY}vh)`
  }else{
    map.style = ''
  }
}