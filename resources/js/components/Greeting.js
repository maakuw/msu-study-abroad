import React, {Component} from 'react'
import cms from '../cms.json'
import {parseProps} from '../settings'

class Greeting extends Component {
  constructor(props){
    super(props)
    
    this.greeting = ''
  }

  componentDidMount(){
    parseProps(this)
  }

  componentDidUpdate(){
    parseProps(this)
  }

  render() {
    return (
      <header data-greeting className={cms.greeting.hStyle}>
        <h1 className={cms.greeting.h1Style}>{this.props.headline ? this.props.headline : this.greeting}</h1>
      </header>
    )
  }
}

export default Greeting