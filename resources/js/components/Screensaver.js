import React, { Component } from "react"
import cms from '../cms.json'

class Screensaver extends Component {
  constructor(props){
    super(props)

    this.state = {
      show: true,
      cycling: false
    }

    this.cycle = this.cycle.bind(this)
    this.uncycle = this.uncycle.bind(this)

    this.resetTimer = this.resetTimer.bind(this)
    this.startTimer = this.startTimer.bind(this)
    this.tickup = this.tickup.bind(this)

    this.liStyle = "w-100 h-100 position-absolute d-block"
    this.cycler = 0
    this.index = 0
    this.timer = 0
    this.currSeconds = 0
    this.delay = 5000
  }

  tickup() {
    this.currSeconds++
    if(!this.props.modal_open && !this.state.show && this.currSeconds === 60) {
      this.setState({show:true})
      this.resetTimer()
      this.cycle()
    }
  }

  startTimer(event=false){
    this.uncycle()
    this.resetTimer()
    if(!this.props.modal_open){
      this.timer = setInterval(this.tickup, this.delay)
      if(this.state.show) this.setState({show:false})
    }
  }
    
  resetTimer() {
    clearInterval(this.timer)
    this.timer = this.currSeconds = 0
  }


  cycle(){
    this.setState({cycling:true})
    let photos = document.querySelectorAll('[data-screensaver] [data-cycle]')
    if(photos.length){
      this.cycler = setInterval(() => {
        photos.forEach(photo => {
          photo.classList.add('opacity-0')
          photo.classList.remove('opacity-100')
        })
        if(this.index >= photos.length){
          this.index = 0
        }
        photos[this.index].classList.add('opacity-100')
        photos[this.index].classList.remove('opacity-0')
        this.index++
      }, this.delay)
    }
  }

  uncycle(){
    clearInterval(this.cycler)
    clearInterval(this.timer)
    this.cycler = this.index = 0
    this.timer = this.currSeconds = 0
    this.setState({cycling:false})
    let photos = document.querySelectorAll('[data-screensaver] [data-cycle]')
    photos.forEach(photo => photo.className = this.liStyle)
  }

  componentDidUpdate(){
    if(this.props.modal_open && this.state.cycling){
      window.removeEventListener('touchstart', this.startTimer)
      window.removeEventListener('click', this.startTimer)
      window.removeEventListener('keypress', this.startTimer)
      this.uncycle()
      this.resetTimer()
      this.setState({show:false})
    }else if(!this.props.modal_open && !this.state.cycling && !this.timer){
      window.ontouchstart = this.startTimer
      window.onclick = this.startTimer
      window.onkeypress = this.startTimer
      if(!this.props.modal_open) this.timer = setInterval(this.tickup, this.delay)
    }
  }

  componentDidMount(){    
    window.ontouchstart = this.startTimer
    window.onclick = this.startTimer
    window.onkeypress = this.startTimer
    this.timer = setTimeout(this.cycle, this.delay)
  }

  render(){
    return (
      <aside data-screensaver 
      className={`position-fixed top-0 left-0 bg-white w-100 h-100 ${!this.state.show ? 'd-none' : 'd-block'}`} >
        <div className="position-absolute w-100 h-100">
          { this.props.screensaver &&
          <ul className="position-relative w-100 h-100 bg-cover bg-top d-flex ps-0"
            style={{listStyle:'none'}}>
            { this.props.screensaver.media.map((image,i) => {
              return (
                <li data-cycle className={this.liStyle + ' ' + (i === 0 ? 'opacity-100' : 'opacity-0')} key={`image--${i}`}>
                  <figure data-backgrounder 
                   style={{backgroundImage:`url(${image.url})`,minHeight:'100%'}}>
                    <img alt="" src={image.url} className="d-block w-100 invisible"/>
                  </figure>
                </li>
              )
            } )}
          </ul>
          }
        </div>
        <figure className="position-absolute w-100 h-100" style={{backgroundColor: 'rgba(255,255,255,0.75)'}}>
          <div className="w-50 h-100 text-pine d-flex flex-column justify-content-center align-items-center mx-auto">
            <svg width="1168" height="873" viewBox="0 0 1168 873" fill="#18453B" className="w-25 h-auto mb-4 mx-auto px-4">
              <use xlinkHref="#logo__msu"/>
            </svg>
            { cms.screensavers.title &&
            <figcaption className="mt-0 text-primary">
              <span className="fw-bold">{cms.screensavers.title}</span>
            </figcaption>
            }
            { cms.screensavers.cta &&
            <nav className="pt-2 mt-4 text-primary">
              <button className="position-relative d-flex h0 text-uppercase border-0 bg-transparent">
                <div>
                  <span style={{textShadow: '0 4px 4px 0 #00000040'}}>{cms.screensavers.cta}</span>
                  <span data-animate="sheen" className="position-absolute z-100 start-0 top-0 w-auto h-auto text-white" 
                   style={{paddingLeft:'6px',paddingTop:'1px'}}>{cms.screensavers.cta}</span>
                </div>
                <svg width="153" height="212" viewBox="0 0 153 212" fill="none" xmlns="http://www.w3.org/2000/svg" className="overflow-visible ms-4">
                  <g>
                    <line data-line6 x1="122" y1="67.5" x2="142" y2="67.5" stroke="#18453B" strokeWidth="7"/>
                    <line data-line5 x1="133.506" y1="35.799" x2="116.185" y2="45.799" stroke="#18453B" strokeWidth="7"/>
                    <line data-line4 x1="108.799" y1="9.99426" x2="98.7987" y2="27.3148" stroke="#18453B" strokeWidth="7"/>
                    <line data-line3 x1="74.5" y1="3.47717e-07" x2="74.5" y2="20" stroke="#18453B" strokeWidth="7"/>
                    <line data-line2 x1="39.7987" y1="8.49426" x2="49.7987" y2="25.8148" stroke="#18453B" strokeWidth="7"/>
                    <line data-line1 x1="13.9941" y1="33.201" x2="31.3146" y2="43.201" stroke="#18453B" strokeWidth="7"/>
                    <line data-line0 x1="4" y1="67.5" x2="24" y2="67.5" stroke="#18453B" strokeWidth="7"/>
                    <circle data-circle cx="73" cy="68" r="32" strokeMiterlimit="10" stroke="#18453B" strokeWidth="7"/>
                    <g data-hand>
                      <path fill="#18453B" d="M67.8,201.3l0.1,0.1c5.3,3.7,6.6,3.7,27.7,3.7h22.7l4.7-2.5c7.5-3.9,14.9-10.5,19.3-17.1
	c2.7-4,4.4-7.7,5.6-12.4l1.1-4l0.1-22.6c-0.1-22.8-0.3-23.5-0.7-25c-0.5-1.7-1.6-3.7-2.8-5c-2.6-3-6.5-4.8-10.4-4.8
	c-2.8,0-5.5,0.8-7.7,2.3c-0.2-0.6-0.5-1.2-0.8-1.9c-2.3-4.4-6.4-7-11.5-7.2c-0.5,0-1,0-1.4,0c-2.1,0-3.8,0.4-5.6,1.3l-0.1,0.1
	c-0.5,0.2-0.9,0.5-1.3,0.8c-0.3-1-0.9-2-1.7-3.2c-2.7-3.8-6.9-6-11.5-6c-2.5,0-4.9,0.7-6.9,1.8c0-2,0-9.4,0-9.4c0-22,0-23.2-1-25.6
	v-0.1c-1.3-3-3.4-5.3-6.4-6.9c-2-1-4.2-1.6-6.5-1.6c-3.7,0-7.1,1.4-9.7,3.9c-0.1,0.1-0.1,0.1-0.2,0.2c-4,3.6-4,3.6-4.2,34.6
	l-0.2,31.6c-0.5-0.5-1.1-1.1-1.7-1.7c-8.8-9.1-10.2-9.9-10.9-10.3l-0.1-0.1c-1.9-1-4-1.5-6.1-1.5c-7.5,0-13.9,6.3-13.9,13.7
	c0,3.6,1.6,6.9,4,10.8c2,3.3,5.3,9.8,10,19.8c0.1,0.2,6.3,11.2,7.9,14.5C58.1,196.7,67.7,201.1,67.8,201.3"/>
                      <path  fill="#FFFFFF" d="M141.9,146.4l-0.1,21.7l-0.8,3.1c-1.1,4-2.4,7-4.7,10.3c-3.7,5.6-10.2,11.3-16.7,14.7l-3.2,1.7H97
	c-22,0-21.7,0-25.4-2.5c-7-4.8-13.9-15.8-25.1-40.2c-5-11-8.6-18.1-10.9-21.8c-2-3.3-2.9-5.3-2.9-7c0-4.8,5.5-8.3,9.7-6
	c0.7,0.4,4.8,4.4,9.4,9.1c4.4,4.6,8.4,8.5,8.7,8.7c1.2,0.7,3,0.5,4-0.4l0.9-0.8l0.2-34.6c0.2-38.3,0-35.3,2.3-37.6
	c2-2,5.3-2.5,8-1.1c1.6,0.8,2.5,1.9,3.2,3.4c0.4,1,0.5,4.1,0.5,22.9c0,24.2-0.1,23.2,2.2,24.2c2.3,1,4.4-0.4,4.8-3.3
	c0.9-6.5,9-8.5,12.7-3.1c1,1.4,1,1.5,1.1,6.7l0.2,5.2l1.1,0.9c1.6,1.4,3.6,1.2,5-0.5c0.3-0.4,0.6-1,0.6-1.5c0-2.5,1.6-5,4-6.3
	c1-0.5,1.8-0.6,3.5-0.5c2.7,0.1,4.6,1.3,5.7,3.5c0.7,1.3,0.7,1.9,0.7,6c0,4.9,0.3,6,1.9,6.7c2.6,1.2,4.7-0.2,5.1-3.3
	c0.7-5.9,8.2-8.2,12.1-3.7c0.6,0.7,1.2,1.8,1.4,2.4C141.9,124.3,142,131.2,141.9,146.4z"/>
                    </g>
                  </g>
                </svg>
              </button>
            </nav>
            }
          </div>
        </figure>
      </aside>
    )
  }
}

export default Screensaver