import cms from '../cms.json'
import React, {Component} from 'react'
import {parseProps} from '../settings'

class Header extends Component {
  constructor(props){
    super(props)
    
    this.title      = ''
    this.department = ''
    this.program    = ''
    this.logo       = ''
    this.has_trigger = false
  }

  componentDidMount(){
    parseProps(this)
  }

  componentDidUpdate(){
    parseProps(this)
    let drawer = document.getElementById('screensaver')
    let trigger = document.querySelector('[data-bs-target="#screensaver"]')
    if(trigger && !this.has_trigger){
      this.has_trigger = true
      drawer.addEventListener('show.bs.offcanvas', function () {
        trigger.classList.add('bg-tertiary')
      })
      drawer.addEventListener('hidden.bs.offcanvas', function () {
        trigger.classList.remove('bg-tertiary')
      })
    }
  }

  render() {
    let is_admin = this.props.user_type === 1
    return (
      <header data-header className={`navbar navbar-light w-100 sticky-top${this.props.hStyle ? ' '+ this.props.hStyle : ''}`}>
        <figure className={`navbar-brand d-flex align-items-center ${this.props.figStyle ? this.props.figStyle : cms.header.figStyle}`}>
          { this.logo &&
          <a href={is_admin ? '/admin' : '/'} aria-label={this.title}
           title="Click to return to home"
           className="d-flex h2 pe-1 mb-0 text-primary text-uppercase">
            <svg className="icon">
              <use xlinkHref="#logo__msu"/>
            </svg>
          </a>
          }
          { this.title &&
          <figcaption className="my-auto">
            <a className="h6 text-primary my-auto" href={is_admin ? '/admin' : '/'}
             title="Click to return to home">
              <>
                <span className="d-none d-sm-inline">
                  <span className="fw-bold">{this.title.substring(0, this.title.lastIndexOf(' '))}</span> {this.title.substring(this.title.lastIndexOf(' ') + 1)}
                </span>
                <span className="d-inline d-sm-none fw-bold">MSU</span>
              </>
            </a>
          </figcaption>
          }
        </figure>
        { ( ( this.department || this.program ) && this.props.show_frontend ) &&
        <div className="d-none d-sm-inline h6 my-auto pe-3 text-primary">{this.department} | <span className="fw-bold text-uppercase">{this.program}</span></div>
        }
        { !this.props.show_frontend &&
        <>
        <button 
          className={`border-0 bg-white h4 mt-auto mb-0 p-2 ${is_admin ? 'me-5 pe-0 ' : ''}position-absolute top-0 end-0 text-pine`} 
          type="button" title="Show/Hide the Attract Loop menu"
          data-bs-toggle="offcanvas" 
          data-bs-target="#screensaver" 
          aria-controls="screensaver" 
          aria-label="Toggle Screensaver/Attract Loop form" 
          style={{height:50}}>
          <svg className="icon">
            <use xlinkHref="#icon__screensaver"/>
          </svg>
        </button>
        { is_admin &&
        <button 
          className="navbar-toggler border-0" 
          type="button" title="Show/Hide the menu"
          data-bs-toggle="offcanvas" 
          data-bs-target="#primary-nav" 
          aria-controls="primary-nav" 
          aria-label="Toggle primary navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        }
        </>
        }
      </header>
    )
  }
}

Header.defaultProps = {
  figStyle: '',
  hStyle: '', 
  show_frontend: false,
  user_type: 'Manager'
}

export default Header