import api from '../api.js'
import cms from '../cms.json'
import {getPaths, parseMedia, timedAlert, updateBodyStyle, updateCountryPrograms} from '../functions'
import {parseSetting, insertSettings} from '../settings'
import { checkContinent, checkCountries, zoomMap } from '../world.js'
import {checkPrograms, parseProgram, injectProgram, updateProgram} from '../programs'
import React, {Component} from "react"
import {BrowserRouter, Route, Routes} from "react-router-dom"
import Header from "../global/Header"
import Footer from "../global/Footer"
import Sidebar from "../global/Sidebar"
import Modal from "../components/Modal"
import Country from "../pages/Country"
import Feedback from "../partials/Feedback"
import Countries from "../pages/Countries"
import Continents from "../pages/Continents"
import Dashboard from "../pages/Dashboard"
import Galleries from "../pages/Galleries"
import Programs from "../pages/Programs"
import Settings from "../pages/Settings"
import firebase from 'firebase/compat/app'
import 'firebase/compat/storage'
import Screensavers from '../pages/Screensavers.js'

// Firebase configuration: measurementId is optional!
const app = firebase.initializeApp({
  apiKey: cms.settings.firebase.api_key,
  authDomain: cms.settings.firebase.auth_domain,
  projectId: cms.settings.firebase.project_id,
  storageBucket: cms.settings.firebase.storage_bucket, 
  messagingSenderId: cms.settings.firebase.messaging_sender_id,
  appId: cms.settings.firebase.app_id,
  measurementId: cms.settings.firebase.measurement_id
})

class Admin extends Component {
  constructor(props) {
    super(props)

    this.state = {
      continents: [], 
      countries: [],
      galleries: [], 
      programs: [], 
      media: [], 
      settings: [], 
      db_path: 'cms', 
      media_types: [], 
      has_server: true,
      loading: false, 
      screensaver: false, 
      user: false,
      modal: {
        headline: '',
        copy: '',
        inputs: false,
        ctas: false
      },
      feedback: {
        msg: false,
        stlye: ''
      },
      countries_have_posted: true,
      programs_have_posted: true,
      path: ''
    }

    //Keep a clean version of everything we pull from the DB
    this.continents = []
    this.countries  = []
    this.country    = false
    this.galleries  = []
    this.programs   = []
    this.media      = []
    this.settings   = []
    this.screensaver = false

    this.resetAlert       = this.resetAlert.bind(this)
    this.checkPage        = this.checkPage.bind(this)
    this.getPage          = this.getPage.bind(this)
    this.parseIt          = this.parseIt.bind(this)

    //Continents
    this.injectCountryToContinent = this.injectCountryToContinent.bind(this)
    this.selectContinent          = this.selectContinent.bind(this)
    this.getContinents            = this.getContinents.bind(this)

    //Country Methods
    this.editCountry      = this.editCountry.bind(this)
    this.enableCountry    = this.enableCountry.bind(this)
    this.getAssociations  = this.getAssociations.bind(this)
    this.getCountry       = this.getCountry.bind(this)
    this.injectCountry    = this.injectCountry.bind(this)
    this.parseCountry     = this.parseCountry.bind(this)
    this.selectCountry    = this.selectCountry.bind(this)
    this.suspendCountry   = this.suspendCountry.bind(this)
    this.getCountries     = this.getCountries.bind(this)

    //Program Methods
    this.getPrograms      = this.getPrograms.bind(this)
    this.getProgramsCountries = this.getProgramsCountries.bind(this)
    this.deleteProgram    = this.deleteProgram.bind(this)
    this.deletePrograms   = this.deletePrograms.bind(this)
    this.editProgram      = this.editProgram.bind(this)
    this.postProgram      = this.postProgram.bind(this)

    //Gallery Methods
    this.createGallery    = this.createGallery.bind(this)
    this.getGalleries     = this.getGalleries.bind(this)
    this.setDetails          = this.setDetails.bind(this)
    this.setQR            = this.setQR.bind(this)

    //Media Methods
    this.getMedia         = this.getMedia.bind(this)
    this.deleteMedia      = this.deleteMedia.bind(this)
    this.searchSubmit     = this.searchSubmit.bind(this)
    this.uploadMedia      = this.uploadMedia.bind(this)
    this.uploadSSMedia    = this.uploadSSMedia.bind(this)
  
    //Modal Methods
    this.resetModal       = this.resetModal.bind(this)
    this.setModal         = this.setModal.bind(this)
    this.editModal        = this.editModal.bind(this)
    this.createModal      = this.createModal.bind(this)
    this.deleteModal      = this.deleteModal.bind(this)

    //Settings
    this.getSettings      = this.getSettings.bind(this)
    this.postSettings     = this.postSettings.bind(this)
    this.getUser          = this.getUser.bind(this)

    this.getScreensaver   = this.getScreensaver.bind(this)
  }

  getScreensaver(callback=false){
    this.setState({loading:true})
    api.get(`/resource/screensaver`)
    .then((response) => {
      this.resetAlert()
      let screensaver = response.data
      screensaver.media = parseMedia(screensaver.media)
      this.setState({
        screensaver: screensaver,
        feedback: {
          msg: 'Screensaver successfully loaded!',
          style: 'success'
        },
      })
      this.screensaver = screensaver
    })
    .finally((vars) => {
      this.setState({loading:false})
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Screensaver! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  setDetails(details, country){
    this.setState({loading: true})
    country.name = details.name
    country.label_x = details.label_x
    country.label_y = details.label_y
    country.line_x1 = details.line_x1
    country.line_y1 = details.line_y1
    country.line_x2 = details.line_x2
    country.line_y2 = details.line_y2
    
    console.log(details, country)
    
    api.put(`/resource/countries/${country.id}`,country)
    .then((response) => {
      let nc = this.injectCountry(response.data)
      let ncc = this.injectCountryToContinent(response.data)
      console.log(ncc, response.data)
      
      this.resetAlert()
      this.setState({
        feedback: {
          msg: `${country.name} updated!`,
          style: 'success'
        },
  //      countries: nc, 
        continents: ncc, 
        loading: false,
        countries_have_posted: true
      })
//      this.countries  = nc
      this.continents = nc
    })
    .catch( error => {
      this.setState({
        feedback: {
          msg: `An error occurred while trying to update the name of ${country.name}: ${error}`,
          style: 'danger'
        },
        countries_have_posted: false
      })
    })
  }

  setQR(url, country){
    this.setState({loading: true})
    country.code = url
    api.put(`/resource/countries/${country.id}`,country)
    .then((response) => {
      let np = this.injectCountry(response.data)
      
      this.resetAlert()
      this.setState({
        feedback: {
          msg: `${country.name} QR code updated!`,
          style: 'success'
        },
        countries: np, 
        loading: false,
        countries_have_posted: true
      })
      this.countries = np
    })
    .catch( error => {
      this.setState({
        feedback: {
          msg: `An error occurred while trying to update the QR code on ${country.name}: ${error}`,
          style: 'danger'
        },
        countries_have_posted: false
      })
    })
  }

  deleteMedia(media){
    this.setState({loading: true})
    let id = media.id
    api.delete(`/resource/media/${id}`)
    .then( response => {
      let nm = this.state.media.filter(m => m.id !== id)  
      firebase.storage().refFromURL(media.url).delete().then(() => {
        // File deleted successfully
        this.resetAlert()
        //Then update the file references
        this.setState({
          feedback: {
            msg: response.data,
            style: 'success'
          },
          media: nm,
          loading: false,
          media_has_posted: true
        })
      }).catch((error) => {
        this.setState({
          feedback: {
            msg: `Uh-oh, an error occurred! :${error}`,
            style: 'danger'
          },
          loading: false,
          media_has_posted: true
        })
      })
    })
    .catch( error => {
      this.setState({
        feedback: {
          msg: `An error occurred while trying to delete "${media.url}" from the database: ${error}`,
          style: 'danger'
        },
        programs_have_posted: false
      })
    })
  }

  uploadMedia(media, gallery_id) {
    this.setState({loading: true})
    let url = cms.settings.firebase.url+media.file.name+'?alt=media'
    let gallery = this.state.galleries.filter(g=>g.id === gallery_id)
    let obj = this.state.media.filter( m => m.url === url && m.gallery_id === gallery_id)
    gallery = gallery.length ? gallery[0] : false
    if(obj.length) {
      firebase.storage().ref(media.file.name).put(media.file)
      this.resetAlert()
      //Then update the file references
      this.setState({
        feedback: {
          msg: `${media.file.name} successfully updated!`,
          style: 'success'
        },
        loading: false,
        media_has_posted: true
      })
    }else{
      obj = {
        gallery_id: gallery_id,
        name: media.file.name,
        type: url.search('.mp4') > 0 ? 2 : 1,
        updated_at: new Date(),
        thumbUrl: url,
        url: url
      }

      api.post(`/resource/media`,obj)
      .then( response => {
        let nm = this.state.media
        obj.id = response.data
        obj.uid = response.data
        obj.status = 'done'
        obj.thumbUrl = url
        obj.url = url
        nm.push(obj)

        this.resetAlert()
        //Then update the file references
        this.setState({
          feedback: {
            msg: `${media.file.name} successfully added to gallery #${gallery_id}!`,
            style: 'success'
          },
          loading: false,
          media_has_posted: true
        })
        firebase.storage().ref(media.file.name).put(media.file).on('state_changed' , (snapshot) => {
          switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED:
              media.file.state = 'paused'
            break;
            case firebase.storage.TaskState.RUNNING:
            default: 
              media.onProgress({percent: (snapshot.bytesTransferred / snapshot.totalBytes) * 100})
            break;
          }
        }, 
        (error) => {
          let state = {
            feedback: {
              style: 'danger'
            }, 
            media: nm, 
            media_has_posted: false
          }
          switch (error.code) {
            case 'storage/unauthorized':
              state.feedback.msg = `An error occurred while trying to create ${media.file.name} because you don't have permission to access this image/video`
            break;
            case 'storage/canceled':
              state.feedback.msg = `Upload cancelled`
              state.feedback.style = 'warning'
            break;
            case 'storage/unknown':
              state.feedback.msg = `An unknown error occurred while trying to create ${media.file.name}, inspect error.serverResponse`
            break;
          }
          this.setState(state)
          this.media = nm
          media.file.state = 'error'
        }, 
        () => {
          this.setState({
            feedback: {
              msg: `${media.file.name} successfully uploaded to Firebase!`,
              style: 'success'
            }
          })

          media.file.thumbUrl = url
          media.onSuccess(media.file)
        })
      })
      .catch( error => {
        media.onError(error)
        this.setState({
          feedback: {
            msg: `${media.file.name} can't be uploaded! ${error.response.data.message}`,
            style: 'danger'
          },
          media_has_posted: false
        })
      })
    }
  }

  uploadSSMedia(media, screensaver_id = 1) {
    this.setState({loading: true})
    let url = cms.settings.firebase.url+media.file.name+'?alt=media'
    let images = this.state.screensaver.media
    let obj = images.filter( m => m.url === url && m.screensaver_id === screensaver_id)
    if(obj.length) {
      firebase.storage().ref(media.file.name).put(media.file)
      this.resetAlert()
      //Then update the file references
      this.setState({
        feedback: {
          msg: `${media.file.name} successfully updated!`,
          style: 'success'
        },
        loading: false,
        media_has_posted: true
      })
    }else{
      obj = {
        screensaver_id: screensaver_id,
        name: media.file.name,
        type: 1,
        updated_at: new Date(),
        thumbUrl: url, 
        url: url
      }

      api.post(`/resource/screensaver`,obj)
      .then( response => {
        let nm = this.state.screensaver
        let ss = response.data
        let img = ss.media[ss.media.length - 1]

        obj.id = img.id
        obj.uid = img.id
        obj.status = 'done'
        obj.thumbUrl = img.url
        obj.url = img.url

        nm.media.push(obj)

        this.resetAlert()
        //Then update the file references
        this.setState({
          feedback: {
            msg: `${media.file.name} successfully added to Screensaver #${screensaver_id}!`,
            style: 'success'
          },
          loading: false,
          screensaver_has_posted: true
        })
        firebase.storage().ref(media.file.name).put(media.file).on('state_changed' , (snapshot) => {
          switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED:
              media.file.state = 'paused'
            break;
            case firebase.storage.TaskState.RUNNING:
            default: 
              media.onProgress({percent: (snapshot.bytesTransferred / snapshot.totalBytes) * 100})
            break;
          }
        }, 
        (error) => {
          let state = {
            feedback: {
              style: 'danger'
            }, 
            screensaver: nm, 
            screensaver_has_posted: false
          }
          switch (error.code) {
            case 'storage/unauthorized':
              state.feedback.msg = `An error occurred while trying to create ${media.file.name} because you don't have permission to access this image/video`
            break;
            case 'storage/canceled':
              state.feedback.msg = `Upload cancelled`
              state.feedback.style = 'warning'
            break;
            case 'storage/unknown':
              state.feedback.msg = `An unknown error occurred while trying to create ${media.file.name}, inspect error.serverResponse`
            break;
          }
          this.setState(state)
          this.screensaver = nm
          media.file.state = 'error'
        }, 
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          this.setState({
            feedback: {
              msg: `${media.file.name} successfully uploaded to Firebase!`,
              style: 'success'
            }
          })
          media.file.url = url
          media.file.thumbUrl = url
          media.onSuccess(media.file)
        })
      })
      .catch( error => {
        media.onError(error)
        this.setState({
          feedback: {
            msg: `${media.file.name} can't be uploaded! ${error.response.data.message}`,
            style: 'danger'
          },
          screensaver_has_posted: false
        })
      })
    }
  }

  parseIt(nc,cb){
    let tag = document.getElementById(`${nc.slug}`)
    let continent = tag ? tag.parentElement : false
    let scale = tag.dataset.scale ? tag.dataset.scale : continent.dataset.scale
    let left = tag.dataset.transformoriginx ? parseFloat(tag.dataset.transformoriginx) : parseFloat(continent.dataset.transformoriginx) + 19
    let top = tag.dataset.transformoriginy ? parseFloat(tag.dataset.transformoriginy) : parseFloat(continent.dataset.transformoriginy)

    if(tag){
      this.setState({
        country: nc,
//        programs: nc.programs, Dev Note: overrides the GLOBAL programs
        gallery: nc.gallery,
//        media: nc.gallery ? nc.gallery.media : [] Dev Note: overrides the GLOBAL media
      })
      cb && cb(nc)
    }else{
      zoomMap(scale, left.toString(), top.toString())
      cb && cb(nc)
    }
  }

  editProgram(program, callback=false){
    program = parseProgram(program)
    this.setState({loading: true})
    api.put(`/resource/programs/${program.id}`,program)
    .then( response => {
      let np = updateProgram(response.data, this)
      let cou = this.state.country
      cou = updateCountryPrograms(cou, response.data)
      this.resetAlert()
      this.setState({
        feedback: {
          msg: `${program.name} successfully updated!`,
          style: 'success'
        },
        country: cou, 
        programs: np, 
        programs_have_posted: true
      })
      this.country = cou
      if(callback) callback(this.state.country)
    })
    .finally((vars) => {
      this.setState({loading:false})
    })
    .catch( error => {
      this.setState({
        feedback: {
          msg: `An error occurred while trying to update ${program.name}: ${error}`,
          style: 'danger'
        },
        programs_have_posted: false
      })
    })
  }

  postProgram(program, callback=false){
    program = parseProgram(program)
    this.setState({loading: true})
    api.post('/resource/programs', program)
    .then( response => {
      let nps = injectProgram(response.data, this)
      let cou = this.state.country
      cou = updateCountryPrograms(cou, response.data)
      this.resetAlert()
      this.setState({ 
        feedback: {
          msg: `${program.name} successfully created!`,
          style: 'success'
        }, 
        programs: nps, 
        country: cou, 
        programs_have_posted: true
      })
      if(callback) callback(this.state.country)
    })
    .finally((vars) => {
      this.setState({loading:false})
    })
    .catch( error => {
      this.setState({
        feedback: {
          msg: `An error occurred while trying to create a program: ${error}`,
          style: 'danger'
        },
        programs_have_posted: false
      })
    })
  }

  deleteProgram(id, callback=false){
    this.setState({loading: true})
    api.delete(`/resource/programs/${id}`)
    .then( response => {
      let np = this.state.programs.filter(p => p.id !== id)
      
      this.resetAlert()
      this.setState({
        feedback: {
          msg: response.data,
          style: 'success'
        },
        programs: np, 
        loading: false,
        programs_have_posted: true
      })
      this.programs = np

      if(callback) callback(np)
    })
    .catch( error => {
      this.setState({
        feedback: {
          msg: `An error occurred while trying to delete the program: ${error}`,
          style: 'danger'
        },
        programs_have_posted: false
      })
    })
  }

  deletePrograms(programs, callback=false){
    let np = this.state.programs
    this.setState({loading: true})
    programs.forEach( pr => {
      api.delete(`/resource/programs/${pr.id}`)
      .then( response => {
        np = this.state.programs.filter(p => p.id !== pr.id)
        this.setState({
          feedback: {
            msg: response.data,
            style: 'success'
          },
          programs: np, 
          programs_have_posted: true
        })
        this.programs = np
        if(callback) callback()
      })
      .catch( error => {
        this.setState({
          feedback: {
            msg: `An error occurred while trying to update Programs: ${error}`,
            style: 'danger'
          },
          programs_have_posted: false
        })
      })
    })

    this.resetAlert()
    this.setState({
      loading: false
    })
  }

  //An Interface for the setModal that will slightly realign the display
  editModal(title, copy, inputs, on_submit) {
    if(this.props){
      this.setModal("edit",title,copy, [
        {
          label: 'Update',
          icon: false
        }
      ], inputs, on_submit)
    }
  }
  
  //An Interface for the setModal that will slightly realign the display
  createModal(title, copy, inputs, on_submit) {
    if(this.props){
      this.setModal("create",title,copy, [
        {
          label: 'Save',
          icon: false
        }
      ], inputs, on_submit)
    }
  }
  
  //An Interface for the setModal that will slightly realign the display
  deleteModal(title, copy, inputs, on_submit) {
    if(this.props){
      this.setModal("delete",title,copy, [
        {
          label: 'Confirm',
          style: 'danger',
          icon: 'math--multiply',
          callback: on_submit
        }
      ], inputs)
    }
  }

  //Set the modal in the state object from this or a child component (modal is reusable)
  setModal(type='edit', headline='', copy='', ctas=false, inputs=[], on_submit=false, image='', caption=false){
    if(type === 'preview') {
      $('#main__modal_window').modal('show')
    }

    this.setState({
      modal: {
        type: type,
        headline: headline,
        copy: copy,
        ctas: ctas,
        image: image, 
        caption: caption, 
        inputs: inputs,
        nStyle: type === 'delete' ? 'justify-content-center' : false,
        on_submit: on_submit
      }
    })
  }

  //Clear out the modal in the state object for this or a child component
  resetModal(){
    $('#main__modal_window').modal('hide')
    setTimeout(() => this.setState({modal: {}}), 300)
  }

  //Triggered onSubmit of form and onChange
  searchSubmit(str='', use_countries=false, use_programs=false){
    let state = {}
    let target = this
    if(str) {
        let query = str.trim().toLowerCase()
        if(use_countries) {
          let filteredCoun = target.countries.filter(r => 
            `${r.id}`.indexOf(query) >= 0 ||
            `${r.name.toLowerCase()}`.indexOf(query) >= 0
          )
          state.countries = filteredCoun
        }
        if(use_programs) {
          let filteredPro = target.programs.filter(r => 
            `${r.id}`.indexOf(query) >= 0 ||
            `${r.name.toLowerCase()}`.indexOf(query) >= 0 ||
            `${r.semester.toLowerCase()}`.indexOf(query) >= 0
          )
          state.programs = filteredPro
        }
    }else{
      if( use_countries ) {
        state.countries = target.countries
      }
      if( use_programs ) {
        state.programs = target.programs
      }
    }
    target.setState(state)
  }

  //Set the alert window to its original state
  resetAlert(msg=false,style=''){
    timedAlert(() => {
      this.setState({
        feedback: {
          msg: msg,
          style: style
        }
      })
    })
  }

  //Basic calls, requiring no login
  getContinents(callback=false) {
    this.setState({loading:true})
    api.get(`/${this.state.db_path}/continents`)
    .then((response) => {
      this.resetAlert()
      this.setState({
        continents: response.data,
        feedback: {
          msg: 'Continents successfully loaded!',
          style: 'success'
        },
        has_server: true
      })
      this.continents = response.data
    })
    .finally((vars) => {
      this.setState({loading:false})
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Continents! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  selectContinent(continent=false,callback=false){
    //console.log('selectContinent', continent)
    if(continent) {
      let countries  = false
      if(this.state.countries) {
        countries = this.state.countries.filter(p => p.continent_id === continent.id)
        countries.forEach(cou => {
          if(!cou.programs) this.getAssociations(cou, (c) => { 
            let dom = document.querySelector('#'+cou.slug)
            let style = checkPrograms(c)
            if(dom) {
              if(!dom.classList.contains('programs')) dom.classList.add(style)
            }
          })
        })
        continent.countries = countries.length > 0 ? countries : false
      }
    }
    this.setState({continent: continent})
    if(callback) callback(continent)
  }

  parseCountry(country) {
    country.id = parseFloat(country.id)
    country.enabled = country.enabled === 'true' ? 'on' : 'off'
    country.continent_id = parseFloat(country.continent_id)
    country.continent = JSON.parse(country.continent)
    country.programs = JSON.parse(country.programs)
    country.gallery_id = country.gallery[0].id
    country.gallery = JSON.parse(country.gallery)
    country.updated_at = new Date()
    return country
  }

  editCountry(country, callback=false){
    country = this.parseCountry(country)
    this.setState({loading: true})
    api.put(`/resource/countries/${country.id}`,country)
    .then((response) => {
      let np = this.injectCountry(response.data)
      this.resetAlert()
      this.setState({
        feedback: {
          msg: `${country.name} updated!`,
          style: 'success'
        },
        countries: np, 
        countries_have_posted: true
      })
      this.countries = np
    })
    .finally((vars) => {
      this.setState({loading:false})
      if(callback) callback()
    })
    .catch( error => {
      this.setState({
        feedback: {
          msg: `An error occurred while trying to toggle suspension on ${country.name}: ${error}`,
          style: 'danger'
        },
        countries_have_posted: false
      })
    })
  }

  enableCountry(country, callback=false){
    country = this.parseCountry(country)
    this.setState({loading: true})
    api.put(`/resource/countries/${country.id}`,country)
    .then((response) => {
      let np = this.injectCountry(response.data)
      this.resetAlert()
      this.setState({
        feedback: {
          msg: `${country.name} ${country.enabled !== 'on' ? 'disabled' : 'enabled'}!`,
          style: country.enabled !== 'on' ? 'success' : 'warning'
        },
        countries: np, 
        countries_have_posted: true
      })
      this.countries = np
    })
    .finally( vars  => {
      this.setState({loading: false})
      if(callback) callback()
    })
    .catch( error => {
      this.setState({
        feedback: {
          msg: `An error occurred while trying to toggle enablement on ${country.name}: ${error}`,
          style: 'danger'
        },
        countries_have_posted: false
      })
    })
  }

  injectCountry(data){
    return this.state.countries.map(cou => {
      if(cou.id === data.id) {
        return data
      }else{
        return cou
      }
    })
  }

  injectCountryToContinent(data){
    return this.state.continents.map(con => {
      con.countries = con.countries.map(cou => {
        if(cou.id === data.id) {
          return data
        }else{
          return cou
        }
      })
      return con
    })
  }

  selectCountry(country=false, callback=false){
    if(country){
      if(!country.programs) {
        //console.log('selectCountry, no programs', country)
        let dom = document.querySelector('#'+country.slug)
        var checkIt = setInterval(() =>{
//          console.log('checkIt for programs', dom.classList)
          if(dom.classList.contains('programs') || dom.classList.contains('noprograms')){
            clearInterval(checkIt)
            this.parseIt(this.state.countries.find((cou) => cou.id === country.id),callback)
          }
        }, 300)
      }else{
        this.parseIt(country,callback)
      }
    }else{
      this.setState({
        country: false,
        programs: false,
        gallery: false, 
        selectedPrograms: []
      })
    }
  }

  suspendCountry(country, callback=false){
    country = this.parseCountry(country)
    this.setState({loading: true})
    api.put(`/resource/countries/${country.id}`,country)
    .then((response) => {
      let np = this.injectCountry(response.data)
      this.resetAlert()
      this.setState({
        feedback: {
          msg: `${country.name} ${country.suspended !== 'on' ? 'reinstated' : 'suspended'}!`,
          style: country.suspended !== 'on' ? 'success' : 'warning'
        },
        countries: np, 
        countries_have_posted: true
      })
      this.countries = np
    })
    .finally( vars  => {
      this.setState({loading: false}) 
      if(callback) callback()
    })
    .catch( error => {
      this.setState({
        feedback: {
          msg: `An error occurred while trying to toggle suspension on ${country.name}: ${error}`,
          style: 'danger'
        },
        countries_have_posted: false
      })
    })
  }

  getCountry(id=false,callback=false){
    this.setState({loading:true})
    api.get(`/${this.state.db_path}/countries/${id}`)
    .then((response) => {
      this.country = response.data
      this.countries.forEach(cou => {
        if(cou.id === this.country.id) cou = this.country
      })
      this.resetAlert()
    })
    .catch((error) => {
      this.setState({
        feedback: {
          msg: `An error occurred when loading the Country! ${error.data}`,
          style: 'danger'
        }
      })
    })
    .finally((vars) => {
      this.setState({
        country: this.country,
        feedback: {
          msg: this.country.name + ' successfully loaded!',
          style: 'success'
        },
        loading:false
      })
      if(callback) callback()
    })
  }

  getAssociations(country, callback = false) {
    this.setState({loading:true})
    let nc
    api.get(`/cms/associations/countries/${country.id}`)
    .then(response => {
      nc = response.data
      let ncs = this.countries.filter(cou => cou.id !== nc.id)
      ncs.push(nc)
      this.countries = ncs
    })
    .finally(() => {
      this.resetAlert()
      this.setState({
        feedback: {
          msg: country.name+' successfully updated with associated programs, galleries, and media!',
          style: 'success'
        }
      })
      this.setState({
        countries: this.countries,
        //programs: nc.programs,
        //gallery: nc.gallery,
        //media: nc.media
      })
      if(callback) callback(nc)
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading associations for ${country.name}! ${error}`,
          style: 'danger'
        }
      })
    })
  }

  getCountries(callback=false){
    this.setState({loading:true})
    api.get(`/${this.state.db_path}/countries`)
    .then((response) => {
      this.countries = response.data
      this.resetAlert()
      this.setState({
        feedback: {
          msg: 'Countries successfully loaded!',
          style: 'success'
        }
      })
    })
    .finally((vars) => {
      this.setState({
        countries: this.countries,
      })
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Countries! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  getPrograms(callback=false){
    this.setState({loading:true})
    api.get(`/${this.state.db_path}/programs`)
    .then((response) => {
      this.resetAlert()
      this.setState({
        programs: response.data,
        feedback: {
          msg: 'Programs successfully loaded!',
          style: 'success'
        }
      })
      this.programs = response.data
    })
    .finally((vars) => {
      this.setState({loading:false})
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Programs! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  getProgramsCountries(program, callback = false) {
    this.setState({loading:true})
    let np
    api.get(`/cms/associations/programs/${program.id}`)
    .then(response => {
      np = response.data
      let nps = this.programs.filter(pro => pro.id !== np.id)
      nps.push(np)
      this.programs = nps
    })
    .finally(() => {
      this.resetAlert()
      this.setState({
        feedback: {
          msg: program.name+' successfully updated with associated countries!',
          style: 'success'
        }
      })
      this.setState({
        programs: this.programs
      })
      if(callback) callback(np)
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading associations for ${program.name}! ${error}`,
          style: 'danger'
        }
      })
    })
  }

  getGalleries(callback=false){
    this.setState({loading:true})
    api.get(`/${this.state.db_path}/galleries`)
    .then((response) => {
      this.resetAlert()
      this.setState({
        galleries: response.data,
        feedback: {
          msg: 'Galleries successfully loaded!',
          style: 'success'
        }
      })
      this.galleries = response.data
    })
    .finally((vars) => {
      this.setState({loading:false})
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Galleries! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  createGallery(id, callback=false){
    this.setState({loading:true})
    let country = this.countries.filter(c => c.id === id)[0]
//    console.log(this.countries, id, country)
    api.post(`/${this.state.db_path}/galleries`, {'countries_id':id})
    .then((response) => {
      this.resetAlert()
      this.galleries = response.data
      country.gallery = this.galleries[this.galleries.length - 1]
      this.countries = this.injectCountry(country)
      console.log(this.countries, this.galleries[this.galleries.length - 1], country)
      this.setState({
        galleries: response.data,
        countries: this.countries, 
        country: country,
        feedback: {
          msg: 'New Gallery created for '+country.name+'!',
          style: 'success'
        }
      })
      if(callback) callback(response.data)
    })
    .finally((vars) => {
      this.setState({loading:false})
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Galleries! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }
  
  getMedia(callback=false){
    this.setState({loading:true})
    api.get(`/${this.state.db_path}/media`)
    .then((response) => {
      let list = parseMedia(response.data)
      this.resetAlert()
      this.setState({
        media: list,
        feedback: {
          msg: 'Media successfully loaded!',
          style: 'success'
        }
      })
      this.media = list
    })
    .finally((vars) => {
      this.setState({loading:false})
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Media! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  getSettings(callback=false){
    this.setState({loading:true})
    api.get(`/resource/settings`)
    .then((response) => {
      this.resetAlert()
      let settings = response.data
      this.setState({
        settings: settings, 
        loading: false,
        feedback: {
          msg: 'Settings successfully loaded!',
          style: 'success'
        },
      })
      this.settings = settings
      insertSettings(settings)
    })
    .finally((vars) => {
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Settings! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  postSettings(settings, callback=false){
    this.setState({loading:true})
    let updates = settings.map(setting => parseSetting(setting, this.state.settings))
    api.post(`/resource/settings`,updates)
    .then((response) => {
      this.resetAlert()
      let settings = response.data
      this.setState({
        settings: settings,
        feedback: {
          msg: 'Settings successfully updated!',
          style: 'success'
        },
      })
      this.settings = settings
      if(callback) callback(settings)
    })
    .finally((vars) => {
      this.setState({loading:false})
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when updating Settings! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  getUser(callback=false){
    this.setState({loading:true})
    api.get('/user')
    .then((response) => {
      this.resetAlert()
      let user = response.data
      let db_path = user ? ( user.user_level_id === 1 ? 'resource' : 'cms' ) : 'resource'
      this.setState({
        user: user,
        loading: false, 
        db_path: db_path,
        feedback: {
          msg: 'User successfully loaded!',
          style: 'success'
        },
      })
    })
    .finally((vars) => {
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading User! ${error.data}`,
          style: 'danger'
        }
      })
    })

    if(callback) callback()
  }

  componentDidMount() {
    updateBodyStyle('admin')
    this.getScreensaver(this.getUser(this.getSettings()))
    this.checkPage()
  }

  getPage(page=false,force=false,country=false) {
    switch(page){
      case 'galleries':
        if(force || (!this.galleries.length & !this.state.loading)) this.getMedia(this.getGalleries(this.getCountries))
        updateBodyStyle('galleries')
      break;
      case 'continents':
        if(force || (!this.continents.length & !this.state.loading)) this.getContinents(this.getCountries)
        updateBodyStyle('continents')
      break;
      case 'programs':
        if(force || !this.programs.length) this.getPrograms()
        updateBodyStyle('programs')
      break;
      case 'settings':
        if(force || (!this.settings.length & !this.state.loading)) this.getSettings()
        updateBodyStyle('settings')
      break;
      case 'countries':
        if(force || (!this.continents.length & !this.state.loading)) this.getContinents(this.getCountries)
        updateBodyStyle('countries')
      break;
      case 'country':
        if(force || !this.country) this.getCountry(country)
        updateBodyStyle('country')
      break;
      default:
        if(force || (!this.continents.length)) this.getContinents(this.getCountries)
        if(force || (!this.media.length)) this.getMedia(this.getPrograms)
        if(force || (!this.galleries.length)) this.getGalleries()
        updateBodyStyle('dashboard')
      break;
    }
  }

  checkPage(force=false){
    let path = ''
    let paths = getPaths()
    let country = false
    if(paths.includes('galleries')){
      path = 'galleries'
    } else if(paths.includes('continents')){
      path = 'continents'
    } else if(paths.includes('programs')){
      path = 'programs'
    } else if(paths.includes('settings')){
      path = 'settings'
    } else {
      if(paths.includes('countries')){
        path = 'countries'
        if(paths.length > 3){
          country = paths[3]
          path = 'country'
          country = paths[3]
        }
      }else{
        path = 'dashboard'
      }
    }
    this.setState({path:path})
    this.getPage(path,force,country)
  }
    
  render(){
    let user_type = this.state.user ? this.state.user.user_level_id : false
    let globalVars = {
      target: this,
      upload: this.state.upload, 
      continents: this.state.continents, 
      country: this.state.country, 
      countries: this.state.countries, 
      gallery: this.state.gallery, 
      galleries: this.state.galleries, 
      programs: this.state.programs, 
      media: this.state.media,
      media_types: this.state.media_types, 
      settings: this.state.settings, 
      user_type: user_type
    }

    let globalMethods = {
      //Alert Methods
      resetAlert: this.resetAlert, 
      //Page Methods
      checkContinent: checkContinent, 
      getPage: this.getPage, 
      //Modal Methods
      editModal: this.editModal, 
      createModal: this.createModal, 
      deleteModal: this.deleteModal, 
      resetModal: this.resetModal, 
      selectContinent: this.selectContinent, 
      setModal: this.setModal, 
      //Search Methods
      searchSubmit: this.searchSubmit
    }

    let countryMethods = {
      checkCountries: checkCountries, 
      editCountry: this.editCountry, 
      enableCountry: this.enableCountry, 
      parseCountry: this.parseCountry,
      selectCountry: this.selectCountry, 
      suspendCountry: this.suspendCountry, 
      setDetails: this.setDetails, 
      setQR: this.setQR
    }

    let mediaMethods = {
      createGallery: this.createGallery, 
      uploadMedia: this.uploadMedia,  
      deleteMedia: this.deleteMedia
    }

    let programMethods = {
      editProgram: this.editProgram, 
      deletePrograms: this.deletePrograms, 
      deleteProgram: this.deleteProgram, 
      postProgram: this.postProgram, 
      getProgramsCountries: this.getProgramsCountries
    }
    
    return (
      <BrowserRouter>
        <Sidebar show_frontend={false} {...globalMethods}/>
        <Header show_frontend={false} hStyle={'bg-white'} {...globalVars}
         user_type={this.state.user ? this.state.user.user_level_id : false} 
         figStyle={"py-0 ps-3 text-start text-uppercase my-auto ms-0 me-auto"}/>
        <Feedback feedback={this.state.feedback}/>
          { user_type === 1 ?
          <Routes>
            <Route path="/admin" element={
              <Dashboard parent={this} {...globalVars} {...globalMethods} {...mediaMethods} {...countryMethods} {...programMethods}/>
            } />
            <Route path="/admin/continents" element={
              <Continents {...globalVars} {...globalMethods}/>
            } />
            <Route path="/admin/countries" element={
              <Countries {...globalVars} {...globalMethods}/>
            } />
            <Route path="/admin/countries/:slug" element={
              <Country {...globalVars} {...globalMethods} {...mediaMethods} {...countryMethods}/>
            } />
            <Route path="/admin/programs" element={
              <Programs {...globalVars} {...globalMethods} {...programMethods}/>
            } />
            <Route path="/admin/galleries" element={
              <Galleries {...globalVars} {...globalMethods} {...mediaMethods}/>
            } />
            <Route path="/admin/settings" element={
              <Settings {...globalVars} {...globalMethods} postSettings={this.postSettings}/>
            } />
          </Routes>
          :
          <Dashboard {...globalVars} {...globalMethods} {...mediaMethods} {...countryMethods} {...programMethods}/>
        }
        <Screensavers {...globalVars} {...globalMethods} {...mediaMethods}
         screensaver={this.state.screensaver}
         deleteMedia={this.deleteMedia} 
         uploadMedia={this.uploadSSMedia}/>
        <Modal id="main__modal_window" {...mediaMethods}
          loading={this.state.loading}
          copy={this.state.modal.copy} 
          headline={this.state.modal.headline}
          image={this.state.modal.image} 
          caption={this.state.modal.caption}
          type={this.state.modal.type}
          ctas={this.state.modal.ctas}
          nStyle={this.state.modal.nStyle} 
          inputs={this.state.modal.inputs} 
          resetModal={this.resetModal} 
          on_submit={this.state.modal.on_submit}/>
        <Footer show_frontend={false} {...globalVars}/>
      </BrowserRouter>
    )
  }
}

export default Admin