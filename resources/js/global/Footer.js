import React, {Component} from 'react'
import cms from "../cms.json"
import {parseProps} from '../settings'

class Footer extends Component {
  constructor(props){
    super(props)
    
    this.footer_text = ''
  }

  componentDidMount(){
    parseProps(this)
  }

  componentDidUpdate(){
    parseProps(this)
  }
  
  render() {
    return (
      <footer data-footer className={`justify-content-center flex-column ${this.props.show_frontend ? 'd-flex' : 'd-none'}`}>
        <div className="container-md d-flex">
          <p className={cms.footer.pStyle}>
            <svg className="icon me-1">
              <use xlinkHref={cms.footer.icon}/>
            </svg>
            <span>{this.footer_text}</span>
          </p>
        </div>
      </footer>
    )
  }
}

export default Footer