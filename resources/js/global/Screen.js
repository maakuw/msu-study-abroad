import api from '../api.js'
import {getPosition, getLinePosition, timedAlert, updateBodyStyle} from '../functions'
import {checkContinent, checkCountries, toggleCountries, zoomMap} from '../world'
import {checkPrograms} from '../programs'
import {insertSettings} from '../settings'
import React, {Component} from "react"
import Header from "../global/Header"
import Footer from "../global/Footer"
import Modal from "../components/Modal"
import Feedback from "../partials/Feedback"
import World from '../components/World'
import Greeting from '../components/Greeting'
import Navigation from '../components/Navigation'
import Screensaver from '../components/Screensaver.js'

class Screen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      continents: [], 
      countries: [],
      galleries: [], 
      programs: [], 
      media: [], 
      media_types: [], 
      continents: [],
      country: false, 
      gallery: false, 
      settings: [], 
      selectedPrograms: [],
      has_server: true,
      loading: false, 
      modal: {
        headline: '',
        copy: '',
        inputs: false,
        ctas: false
      },
      feedback: {
        msg: false,
        stlye: ''
      },
      slides: false
    }

    this.countries = []
    this.programs = []
    this.settings = []

    //Global Methods
    this.resetAlert       = this.resetAlert.bind(this)
    this.getUser          = this.getUser.bind(this)
    this.parseIt          = this.parseIt.bind(this)

    // Continents Methods
    this.selectContinent  = this.selectContinent.bind(this)
    this.getContinents    = this.getContinents.bind(this)

    //Country Methods
    this.getAssociations  = this.getAssociations.bind(this)
    this.getCountries     = this.getCountries.bind(this)
    this.selectCountry    = this.selectCountry.bind(this)

    // Program Methods
    this.getPrograms      = this.getPrograms.bind(this)

    // Gallery Methods
    this.getGalleries     = this.getGalleries.bind(this)

    //Media Methods
    this.getMedia         = this.getMedia.bind(this)
  
    //Modal Methods
    this.resetModal       = this.resetModal.bind(this)
    this.setModal         = this.setModal.bind(this)

    this.getCoor          = this.getCoor.bind(this)
    this.getScreensaver   = this.getScreensaver.bind(this)
    this.getSettings      = this.getSettings.bind(this)
  }

  parseIt(nc){
    let tag = document.getElementById(`${nc.slug}`)
    let continent = tag ? tag.parentElement : false
    this.setState({
      country: nc, 
      selectedPrograms: [], 
      programs: nc.programs,
      gallery: nc.gallery
    })
    let scale = tag.dataset.scale ? tag.dataset.scale : continent.dataset.scale
    let left = tag.dataset.transformoriginx ? parseFloat(tag.dataset.transformoriginx) : parseFloat(continent.dataset.transformoriginx) + 19
    let top = tag.dataset.transformoriginy ? parseFloat(tag.dataset.transformoriginy) : parseFloat(continent.dataset.transformoriginy)
    zoomMap(scale, left.toString(), top.toString())
  }

  selectContinent(continent=false,callback=false){
    if(continent) {
      let countries  = false
      if(this.countries) {
        countries = this.countries.filter(p => p.continent_id === continent.id)
        countries.forEach(cou => {
          if(!cou.programs) this.getAssociations(cou, (c) => { 
            let dom = document.querySelector('#'+cou.slug)
            let style = checkPrograms(c)
            if(dom) {
              if(!dom.classList.contains('programs')) dom.classList.add(style)
            }
          })
        })
        continent.countries = countries.length > 0 ? countries : false
      }
    }
    this.setState({continent: continent})
    if(callback) callback(continent)
  }

  selectCountry(country=false){
    if(country){
      if(!country.programs) {
        this.getAssociations(country, this.parseIt)
      }else{
        this.parseIt(country)
      }
    }else{
      this.setState({
        country: false,
        programs: false,
        gallery: false, 
        selectedPrograms: []
      })
    }
  }

  //Clear out the modal in the state object for this or a child component
  resetModal(){
    let video = document.getElementById('modal_video')
    if( video ) video.pause()
    this.setState({modal: {}})
  }

  //Set the modal in the state object from this or a child component (modal is reusable)
  setModal(type='edit', headline='', copy='', ctas=false, inputs=[], on_submit=false, image='', credit=false){
    if(type === 'preview') {
      $('#main__modal_window').modal('show')
    }
    this.setState({
      modal: {
        type: type,
        headline: headline,
        copy: copy,
        ctas: ctas,
        image: image, 
        credit: credit, 
        inputs: inputs,
        nStyle: type === 'delete' ? 'justify-content-center' : false,
        on_submit: on_submit
      }
    })
  }

  //Set the alert window to its original state
  resetAlert(msg=false,style=''){
    timedAlert(() => {
      this.setState({
        loading: false,
        feedback: {
          msg: msg,
          style: style
        }
      })
    })
  }

  //Basic calls, requiring no login
  getContinents(give_feedback=true) {
    this.setState({loading:true})
    api.get('/cms/continents')
    .then(response => {
      if(give_feedback) {
        this.resetAlert()
        this.setState({
          feedback: {
            msg: 'Continents successfully loaded!',
            style: 'success'
          },
        })
      }
      this.setState({
        continents: response.data,
        has_server: true
      })
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Continents! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  getCountries(give_feedback=true){
    this.setState({loading:true})
    api.get('/cms/countries')
    .then(response => {
      this.countries = response.data
    })
    .finally(() => {
      if(give_feedback) {
        this.resetAlert()
        this.setState({
          feedback: {
            msg: 'Countries successfully loaded!',
            style: 'success'
          }
        })
      }
      this.setState({
        countries: this.countries
      })
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Countries! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  getAssociations(country, callback = false) {
    this.setState({loading:true})
    let nc
    api.get(`/cms/associations/countries/${country.id}`)
    .then(response => {
      nc = response.data
      let ncs = this.countries.filter(cou => cou.id !== nc.id)
      ncs.push(nc)
      this.countries = ncs
    })
    .finally(() => {
      this.resetAlert()
      this.setState({
        feedback: {
          msg: country.name+' successfully updated with associated programs, galleries, and media!',
          style: 'success'
        }
      })
      this.setState({
        countries: this.countries,
        programs: nc.programs,
        gallery: nc.gallery,
        media: nc.media
      })
      if(callback) callback(nc)
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading associations for ${country.name}! ${error}`,
          style: 'danger'
        }
      })
    })
  }

  getPrograms(){
    this.setState({loading:true})
    api.get('/cms/programs')
    .then(response => {
      this.resetAlert()
      this.setState({
        programs: response.data,
        feedback: {
          msg: 'Programs successfully loaded!',
          style: 'success'
        },
        is_loaded: true
      })
      this.programs = response.data
    })
    .finally(
      updateBodyStyle()
    )
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Programs! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  getGalleries(callback=false){
    this.setState({loading:true})
    api.get('/cms/galleries')
    .then(response => {
      this.resetAlert()
      this.setState({
        galleries: response.data,
        feedback: {
          msg: 'Galleries successfully loaded!',
          style: 'success'
        }
      })
      //Push the galleries we just got into the existing Countries
      if(this.countries.length){
        this.countries.forEach(cou => {
          let gall = response.data.filter(g => g.country_id === cou.id)
          cou.gallery = gall.length ? gall[0] : false
        })
        this.setState({countries: this.countries})
        if(callback) callback()
      }
    })
    .finally(() => {
        updateBodyStyle()
      }
    )
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Galleries! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }
  
  getMedia(){
    this.setState({loading:true})
    api.get('/cms/media')
    .then(response => {
      let list = response.data.map(media => {
        return ( {
          id: media.id,
          uid: media.id,
          name: 'no name',
          status: 'done',
          gallery_id: media.gallery_id, 
          caption: media.caption, 
          poster: media.poster, 
          thumbUrl: media.thumbUrl, 
          url: media.url
        } )
      })
      //Push the galleries we just got into the existing Countries
      if(this.countries){
        this.countries.forEach(cou => {
          if(cou.gallery) {
            let media = list.filter(m => m.gallery_id === cou.gallery.id)
            cou.media = media.length ? media : false
          }
        })
        this.resetAlert()
        this.setState({
          media: list,
          countries: this.countries, 
          feedback: {
            msg: 'Media successfully loaded!',
            style: 'success'
          },
          is_loaded: true
        })
      }
    })
    .finally( updateBodyStyle() )
    .catch(error => {
      this.setState({
        feedback: {
          msg: `An error occurred when loading Media! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  getUser(callback=false){
    this.setState({loading:true})
    api.get('/user')
    .then(response => {
      this.resetAlert()
      this.setState({
        user: response.data,
        feedback: {
          msg: 'User successfully loaded!',
          style: 'success'
        },
        is_loaded: true
      })
    })
    .finally(vars => {
      updateBodyStyle()
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading User! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  getCoor(slug, x){
    return getPosition(this,slug,x)
  }

  getLine(slug){
    return getLinePosition(this,slug)
  }

  getScreensaver(callback=false){
    this.setState({loading:true})
    api.get(`/cms/screensaver`)
    .then(response => {
      this.resetAlert()
      let screensaver = response.data
      this.setState({
        screensaver: screensaver,
        feedback: {
          msg: 'Screensaver successfully loaded!',
          style: 'success'
        },
      })
      this.screensaver = screensaver
    })
    .finally(vars => {
      this.setState({loading:false})
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Screensaver! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  getSettings(callback=false){
    this.setState({loading:true})
    api.get(`/cms/settings`)
    .then(response => {
      this.resetAlert()
      let settings = response.data
      this.setState({
        settings: settings,
        feedback: {
          msg: 'Settings successfully loaded!',
          style: 'success'
        },
      })
      this.settings = settings
      insertSettings(settings)
    })
    .finally(vars => {
      this.setState({loading:false})
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Settings! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  componentDidUpdate(){
    let slides = document.querySelectorAll('.slick-slide:not(.slick-clone)')
    if(slides && !this.state.slides) {
      slides.forEach(sl => {
        sl.classList.add('btn')
        sl.addEventListener('click', (e) => {
          let target = e.target.querySelector('img')
          if(target) this.setModal("preview",'','', [], false, false, target.dataset.src, target.title)
        })
      })
    }
  }

  componentDidMount() {
    this.getUser(this.getSettings)
    this.getContinents()
    this.getCountries()
    this.getGalleries(this.getMedia)
    this.getPrograms()
    this.getScreensaver()
  }

  render(){
    let levels = []
    if(this.state.continent) levels.push(this.state.continent)
    if(this.state.country) levels.push(this.state.country)

    let globalVars = {
      settings: this.state.settings, 
      user_type: this.state.user ? this.state.user.user_level_id : false
    }

    return (
      <>
        <Header {...globalVars} show_frontend={true} user_type={this.state.user ? this.state.user.user_level_id : false}/>
        <Feedback {...globalVars} feedback={this.state.feedback}/>
        <article key="map__wrapper" 
        className="d-flex m-0 px-0 align-items-stretch justify-content-stretch h-100 w-100 flex-column bg-aluminum">
          <Greeting {...globalVars}/>
          <Navigation levels={levels} selectCountry={this.selectCountry} 
          toggleCountries={toggleCountries} selectContinent={this.selectContinent}/>
          <World show_suspended={false} continent={this.state.continent} continents={this.state.continents}
            country={this.state.country} countries={this.state.countries} programs={this.state.programs} 
            selectCountry={this.selectCountry} selectContinent={this.selectContinent}
            checkCountries={(slug) => { return checkCountries(slug,this)}}
            checkContinent={(slug) => { return checkContinent(slug,this)}} 
            getCoor={this.getCoor} getLine={this.getLine}/>
        </article>
        <Modal 
          id="main__modal_window"  
          loading={this.state.loading}
          copy={this.state.modal.copy} 
          headline={this.state.modal.headline}
          image={this.state.modal.image} 
          credit={this.state.modal.credit} 
          type={this.state.modal.type}
          ctas={this.state.modal.ctas}
          nStyle={this.state.modal.nStyle} 
          inputs={this.state.modal.inputs} 
          resetModal={this.resetModal} 
          deleteMedia={this.deleteMedia}  
          uploadMedia={this.uploadMedia} 
          on_submit={this.state.modal.on_submit}/>
        <Footer show_frontend={true} {...globalVars}/>
        <Screensaver screensaver={this.state.screensaver}/>
      </>
    )
  }
}

export default Screen
