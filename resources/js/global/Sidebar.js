import cms from '../cms.json'
import React, {Component} from 'react'
import { NavLink } from "react-router-dom"

class Sidebar extends Component {
  render() {
    let nav = document.querySelector('[data-bs-target="#primary-nav"]')
    return (
      !this.props.show_frontend ?
      <aside id="primary-nav" data-sidebar 
      data-bs-backdrop="false" 
      className={`offcanvas offcanvas-end navbar navbar-dark sticky-top flex-column justify-content-start align-items-center${this.props.show_ui ? ' d-none' : ''}`}>
        <div className="offcanvas-body bg-primary w-100">
          <ul className="nav navbar-nav ms-0 d-grid gap-1 bg-white pb-1">
          { cms.routes.map((link,l) => {
            return (
            <li key={`nav-link--${l}`} className="py-0 nav-link fw-bold text-uppercase">
              <NavLink onClick={() => {
                //Dev Note: This breaks the interaction. See Bootstrap 5.0 interactions to determine a similar but better way --MW
                nav.click()
                this.props.getPage(cms[link].slug,true)
              }} 
              className="btn btn-sm btn-primary rounded-0 w-100" 
              data-bs-toggle="tooltip" data-bs-placement="left"
              data-bs-original-title={cms[link].tooltip} title={cms[link].tooltip} 
              to={'/admin/'+cms[link].slug}>{cms[link].label}
              </NavLink>
            </li>
            )
          } ) }
          </ul>
        </div>
      </aside>
      :
      ''
    )
  }
}

Sidebar.defaultProps = {
  show_frontend: true,
  getPage: () => {}
}

export default Sidebar