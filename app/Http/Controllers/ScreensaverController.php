<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Screensaver;
use App\Models\Media;
use Illuminate\Http\Request;

class ScreensaverController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $screensaver = Screensaver::find(1);
        $screensaver->media;
        
        return response()->json($screensaver);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'url' => 'url',
            'screensaver_id' => 'nullable'
        ]);

        $media = new Media;
        $media->url = $request->get('url');
        $media->type = $request->get('type');
        $media->screensaver_id = $request->get('screensaver_id');

        $media->save();
        
        $screensaver = Screensaver::find(1);
        $screensaver->media;
        $screensaver->save();

        return response()->json($screensaver);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Screensaver::find($id);
        $gallery->delete();
        
        return response()->json('Screensaver successfully deleted!');
    }
}
