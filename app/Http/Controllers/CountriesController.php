<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Countries;
use App\Events\CountriesUpdated;
use Illuminate\Http\Request;

class CountriesController extends Controller
{

    public function parse($id)
    {
        $country = Countries::find($id);
        $country->continent;
        $country->programs;
        $country->gallery;
        if($country->gallery) $country->gallery->media;

        return response()->json($country);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Countries::all();

        return response()->json($countries);//this->parse($countries,'listing'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //Can this be bundled for optimization?
        $country = Countries::where('slug', '=', $slug)->get();
        $country[0]->programs;
        $country[0]->gallery;
        if($country[0]->gallery) $country[0]->gallery->media;

        return response()->json($country[0]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function active()
    {
        $countries = Countries::where('enabled','on')->get();

        return response()->json($countries);//this->parse($countries,'listing'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'slug' => 'required',
            'name' => 'required'
        ]);

        $country = new Countries([
            'slug' => $request->get('slug'),
            'name' => $request->get('name'),
            'continent_id' => $request->get('parent'), 
            'gallery_id' => $request->get('gallery'), 
            'code' => $request->get('code'), 
            'enabled' => $request->get('enabled'),
            'suspended' => $request->get('suspended'),
            'label_x' => $request->get('label_x'),
            'label_y' => $request->get('label_y'),
            'line_x1' => $request->get('line_x1'),
            'line_y1' => $request->get('line_y1'),
            'line_x2' => $request->get('line_x2'),
            'line_y2' => $request->get('line_y2')
        ]);

        $country->save();

        return response()->json('Country successfully created!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'slug' => 'required'
        ]);

        $country            = Countries::find($id);
        $country->name      = $request->get('name');
        $country->color     = $request->get('color');
        $country->code      = $request->get('code');
        $country->enabled   = $request->get('enabled');
        $country->suspended = $request->get('suspended');
        $country->label_x   = $request->get('label_x');
        $country->label_y   = $request->get('label_y');
        $country->line_x1   = $request->get('line_x1');
        $country->line_y1   = $request->get('line_y1');
        $country->line_x2   = $request->get('line_x2');
        $country->line_y2   = $request->get('line_y2');

        $country->save();
//        $country = $this->parse([$country],'update');
        $country->continent;
        $country->programs;
        $country->gallery;
        if($country->gallery) $country->gallery->media;

        return response()->json($country);//[0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $continent = Countries::find($id);
        $continent->delete();
        
        return response()->json('Country successfully deleted!');
    }
}
