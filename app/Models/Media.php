<?php

namespace App\Models;

use App\Models\Gallery;
use App\Models\Screensaver;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = ([
        'url', 
        'thumbUrl', 
        'type',  
        'gallery_id', 
        'screensaver_id', 
        'credit', 
        'poster'
    ]);

    public function gallery() {
        return $this->belongsToOne(Gallery::class);
    }

    public function screensaver() {
        return $this->belongsToOne(Screensaver::class);
    }
}
