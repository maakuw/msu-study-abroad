<?php

namespace App\Models;

use App\Models\Media;
use Illuminate\Database\Eloquent\Model;

class Screensaver extends Model
{

    public function media() {
        return $this->hasMany(Media::class);
    }
}
